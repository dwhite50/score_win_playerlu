Unit formTDInfo;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, dbSizes, PVIO, VListBox, Clubfees, history, mtables,
  groups,hh;

Type
  { Required for Util2.Player_Check Function }
  OpenString = String[10];
  _DateString = String[8];

Type
  TfrmTDInfo = Class(TForm)
    StatusBar1: TStatusBar;
    GBPlayerInfo: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Shape1: TShape;
    Label3: TLabel;
    Shape2: TShape;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Shape3: TShape;
    Label11: TLabel;
    Shape4: TShape;
    Shape12: TShape;
    Shape13: TShape;
    Shape14: TShape;
    LMDHLCBState: TLMDHeaderListComboBox;
    LMDHLCBCountry: TLMDHeaderListComboBox;
    OSFLastName: TOvcSimpleField;
    OSFFirstName: TOvcSimpleField;
    OSFPlayerNo: TOvcSimpleField;
    OSFStreet1: TOvcSimpleField;
    OSFStreet2: TOvcSimpleField;
    OSFCity: TOvcSimpleField;
    OSFEmail: TOvcSimpleField;
    OPFZip: TOvcPictureField;
    LMDButPlayerInfoDone: TLMDButton;
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF3Key: TButton;
    StaticTextF3Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    Button1: TButton;
    StaticText3: TStaticText;
    ImageList1: TImageList;
    ActionManager3: TActionManager;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    LMDHint1: TLMDHint;
    OvcController1: TOvcController;
    ResizeKit1: TResizeKit;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    Label12: TLabel;
    OSFTDNumber: TOvcSimpleField;
    Label13: TLabel;
    OSFTDRank: TOvcSimpleField;
    Label14: TLabel;
    OSFEmployeeStatus: TOvcSimpleField;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    OPFPhone: TOvcPictureField;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure CtrlCKeyExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure pFillTDFields;
    Procedure pFirstTDRecord;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure pNextTDRecord;
    Procedure pPlayerNFillFields;
    Procedure pPlayerNBlankFields;
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure pPrevTDRecord;
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure F3KeyExecute(Sender: TObject);
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure OSFPlayerNoExit(Sender: TObject);
    Procedure OSFLastNameExit(Sender: TObject);
    Procedure pSaveTDChanges;
    Procedure pAbortTDChanges;
    Procedure OSFTDNumberExit(Sender: TObject);
    Procedure OSFTDRankExit(Sender: TObject);
    Procedure OSFEmployeeStatusExit(Sender: TObject);
    Function fcheck_tdplayer(Var Player: OpenString): boolean;
    Function fcheck_tdnumber(Const tdn: String): boolean;
    Function fValidEmployeeStatus(Const Field: String): boolean;
    Function fPlayerFound(Const number: String; Const Fno, Kno: byte): boolean;
    Function fduplicate_player(Const number: String; Const Fno, Kno: byte): boolean;
    Function fKeyFound(Const Keytofind: String; Const Fno, Kno: byte; Const CheckRecno: boolean):
      boolean;
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure pTDInfoGB(bEnable: Boolean);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure pCopyTDRecData;
    Procedure pUpdateStatusBar;
    Function fCheckForTDChanges: Boolean;
    Procedure pCheckTDEditFields;
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pLookForTdRec;
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure Action3Execute(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    Procedure Action6Execute(Sender: TObject);
    Procedure Action7Execute(Sender: TObject);
    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    Procedure pResetFldAttrib;
    Procedure OSFPlayerNoEnter(Sender: TObject);
    Procedure OSFLastNameEnter(Sender: TObject);
    Procedure OSFFirstNameEnter(Sender: TObject);
    Procedure OSFFirstNameExit(Sender: TObject);
    Procedure OSFTDNumberEnter(Sender: TObject);
    Procedure OSFTDRankEnter(Sender: TObject);
    Procedure OSFEmployeeStatusEnter(Sender: TObject);
    function FormHelp(Command: Word; Data: Integer;
      var CallHelp: Boolean): Boolean;
  private
    { Private declarations }
  public
    { Public declarations }
  End;

Var
  frmTDInfo: TfrmTDInfo;
  iTDReccount: Integer;
  ksTDInfo, ksTDInfoTemp: keystr;
  ksPN, ksPNTemp: keystr;
  ksFirstTDKey: KeyStr;
  PlayerError: byte;

Implementation

Uses formPlayerLookUp;

{$DEFINE indatabase}
{$DEFINE isnoattend}
{$I STSTRS.dcl}
{$I showpts.inc}
{$I addform.inc}
{$I fixdb.inc}
{$I I_Player.inc}

{$R *.dfm}

Procedure TfrmTDInfo.ButQuitActionExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmTDInfo.CtrlCKeyExecute(Sender: TObject);
Begin
  frmPlayerLookUp.CtrlCKeyExecute(Sender);
End;

Procedure TfrmTDInfo.FormActivate(Sender: TObject);
Begin
  { Must be set to true for Win32 applications. Used in Dbase and Fdbase }
  UseDBLinkages := True;
  UseLookupDB := True;
  fDBUseLinks := True;
  Application.HintHidePause := 200000;
  Scrno := 2;
  //==========================================================
  { These variables are declared in CEDef.pas }
  IniName := GetCurrentDir + '\' + DefIniName;
  AppName := 'TDInfo';
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is made to a "Record",
    dbase re-writes the table header and in Playern.dat, updates dbLastImpDate entry. Set them to
    False when you Delete a record, then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  //==========================================================
  iDataMode := 0;
  sDataMode := null;
  //==========================================================
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ----------------------------------------------------------------------------------------------------------
  { Store the number of records in TDInfo.DAT to iReccount }
  iTDReccount := db33.UsedRecs(fDatF[TDInfo_no]^);
  // ===========================================================\
  { Set the Dbase.pas variable Filno to Table Number we are working with, Filno is used Globally }
  fFilno := TDInfo_no;
  // pFirstTDRecord;
  pLookForTdRec;
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(fPrepend + 'TDINFO.DAT');
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iTDReccount)) + ' Records.';
  pFillTDFields;
  If bWinNormal Then WindowState := wsNormal
  Else WindowState := wsMaximized;
End;

Procedure TfrmTDInfo.pFillTDFields;
Begin
  OSFPlayerNo.Text := Trim(TDInfo.PLAYER_NO);
  OSFLastName.Text := Trim(TDInfo.LAST_NAME);
  OSFFirstName.Text := Trim(TDInfo.FIRST_NAME);
  OSFTDNumber.Text := TDInfo.TD_NO;
  OSFTDRank.Text := Trim(TDInfo.TD_RANK);
  OSFEmployeeStatus.Text := Trim(TDInfo.EMPL_STAT);
End;

Procedure TfrmTDInfo.pFirstTDRecord;
Begin
  ClearKey(fIdxKey[fFilno, 1]^);
  //SkipNextf(fFilno, ksFirstTDKey);
  pNextTDRecord; // Move the record pointer in the table to the next available record and load the data in the screen fields
End;

Procedure TfrmTDInfo.ButNextActionExecute(Sender: TObject);
Begin
  pNextTDRecord; // Move the record pointer in the table to the next available record and load the data in the screen fields
  If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
End;

Procedure TfrmTDInfo.pNextTDRecord;
Begin
  { ksTDInfo, ksTDInfoTemp: keystr;}
  NextKey(fIdxKey[fFilno, 1]^, fRecno[fFilno], ksTDInfoTemp);
  If OK Then Begin
    // ------------------------------------------------------------
    GetRec(fDatF[fFilno]^, fRecno[fFilno], Tdinfo);
    fGetARec(fFilno);
    // ------------------------------------------------------------
    ksTDInfo := fGetKey(fFilno, 1);
    // ------------------------------------------------------------
    ksPN := fGetKey(fFilno, 2);
    ksPNTemp := ksPN;
    // ------------------------------------------------------------
    { Search for records in the player group table that match the Player Number key of the current Player Number record }
    FindKey(IdxKey[Playern_no, 2]^, Recno[Playern_no], ksPNTemp);
    { While the Player Number Key matches, store this players groups to the string grid for viewing }
    If ok And CompareKey(ksPNTemp, ksPN) Then Begin
      GetRec(DatF[Playern_no]^, Recno[Playern_no], PlayerN);
      pPlayerNFillFields;
    End
    Else Begin
      pPlayerNBlankFields;
    End;
    pFillTDFields;
  End
  Else Begin
    { Move the record pointer in the table to the previous record and load the data in the screen
      fields }
    pPrevTDRecord;
  End;
End;

Procedure TfrmTDInfo.pPlayerNFillFields;
Begin
  OSFStreet1.Text := Playern.STREET1;
  OSFStreet2.Text := Playern.STREET2;
  OSFCity.Text := Playern.CITY;
  // STATE   ----------------------------------------------------------------------
  LMDHLCBState.Text := Playern.STATE;
  // ------------------------------------------------------------------------------
  OPFZip.Text := Playern.ZIP;
  // COUNTRY ----------------------------------------------------------------------
  LMDHLCBCountry.Text := Playern.COUNTRY;
  // ------------------------------------------------------------------------------
  OSFEmail.Text := Playern.EMAIL;
  // ------------------------------------------------------------------------------
  OPFPhone.Text := Playern.PHONE;
End;

Procedure TfrmTDInfo.pPlayerNBlankFields;
Begin
  OSFStreet1.Text := '';
  OSFStreet2.Text := '';
  OSFCity.Text := '';
  // STATE   ----------------------------------------------------------------------
  LMDHLCBState.Text := '';
  // ------------------------------------------------------------------------------
  OPFZip.Text := '';
  // COUNTRY ----------------------------------------------------------------------
  LMDHLCBCountry.Text := '';
  // ------------------------------------------------------------------------------
  OSFEmail.Text := '';
  // ------------------------------------------------------------------------------
  OPFPhone.Text := '';
End;

Procedure TfrmTDInfo.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevTDRecord; // Move the record pointer in the table to the previous record and load the data in the screen fields
  If ButPrev.Enabled Then ButPrev.SetFocus Else ButQuit.SetFocus;
End;

Procedure TfrmTDInfo.pPrevTDRecord;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen
    fields }
  PrevKey(fIdxKey[fFilno, 1]^, fRecno[fFilno], ksTDInfoTemp);
  If OK Then Begin
    GetRec(fDatF[fFilno]^, fRecno[fFilno], Tdinfo);
    fGetARec(fFilno);
    // ------------------------------------------------------------
    ksTDInfo := fGetKey(fFilno, 1);
    // ------------------------------------------------------------
    ksPN := fGetKey(fFilno, 2);
    ksPNTemp := ksPN;
    // ------------------------------------------------------------
    { Search for records in the player group table that match the Player Number key of the current Player Number record }
    FindKey(IdxKey[Playern_no, 2]^, Recno[Playern_no], ksPNTemp);
    { While the Player Number Key matches, store this players groups to the string grid for viewing }
    If ok And CompareKey(ksPNTemp, ksPN) Then Begin
      GetRec(DatF[Playern_no]^, Recno[Playern_no], PlayerN);
      pPlayerNFillFields;
    End
    Else Begin
      pPlayerNBlankFields;
    End;
    pFillTDFields;
  End
  Else pNextTDRecord;
End;

Procedure TfrmTDInfo.ButTopActionExecute(Sender: TObject);
Begin
  pFirstTDRecord;
End;

Procedure TfrmTDInfo.ButLastActionExecute(Sender: TObject);
Begin
  ClearKey(fIdxKey[fFilno, 1]^);
  PrevKey(fIdxKey[fFilno, 1]^, fRecno[fFilno], ksTDInfoTemp);
  // ------------------------------------------------------------
  GetRec(fDatF[fFilno]^, fRecno[fFilno], Tdinfo);
  fGetARec(fFilno);
  // ------------------------------------------------------------
  ksTDInfo := fGetKey(fFilno, 1);
  // ------------------------------------------------------------
  ksPN := fGetKey(fFilno, 2);
  ksPNTemp := ksPN;
  // ------------------------------------------------------------
  { Search for records in the player group table that match the Player Number key of the current Player Number record }
  FindKey(IdxKey[Playern_no, 2]^, Recno[Playern_no], ksPNTemp);
  { While the Player Number Key matches, store this players groups to the string grid for viewing }
  If ok And CompareKey(ksPNTemp, ksPN) Then Begin
    GetRec(DatF[Playern_no]^, Recno[Playern_no], PlayerN);
    pPlayerNFillFields;
  End
  Else Begin
    pPlayerNBlankFields;
  End;
  pFillTDFields;
End;

Procedure TfrmTDInfo.ButFindActionExecute(Sender: TObject);
Begin
  { OK must be set to true before calling the Find_Record function }
  Ok := True;
  fFind_Record('', '', ' Select TD Record ', MC, 0, True);
  //    ksTDInfo := fGetKey(fFilno, 1);
  //    pPlayerNFillFields;
  //    pFillTDFields;
  //    Refresh;
    // ------------------------------------------------------------
  GetRec(fDatF[fFilno]^, fRecno[fFilno], Tdinfo);
  fGetARec(fFilno);
  // ------------------------------------------------------------
  ksTDInfo := fGetKey(fFilno, 1);
  // ------------------------------------------------------------
  ksPN := fGetKey(fFilno, 2);
  ksPNTemp := ksPN;
  // ------------------------------------------------------------
  { Search for records in the player group table that match the Player Number key of the current Player Number record }
  FindKey(IdxKey[Playern_no, 2]^, Recno[Playern_no], ksPNTemp);
  { While the Player Number Key matches, store this players groups to the string grid for viewing }
  If ok And CompareKey(ksPNTemp, ksPN) Then Begin
    GetRec(DatF[Playern_no]^, Recno[Playern_no], PlayerN);
    pPlayerNFillFields;
  End
  Else Begin
    pPlayerNBlankFields;
  End;
  pFillTDFields;
End;

Procedure TfrmTDInfo.F1KeyExecute(Sender: TObject);
Begin
  //pRunExtProg(frmTDInfo, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmTDInfo.F3KeyExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmTDInfo.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  //If key = VK_RETURN Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  If key = VK_RETURN Then Begin
    key := 0;
    If iDataMode > 0 Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortTDChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    { At least the Last Name Field must be filled in... }
    If iDataMode > 0 Then If Trim(OSFLastName.Text) <> '' Then pSaveTDChanges Else pAbortTDChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmTDInfo.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortTDChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    { At least the Last Name Field must be filled in... }
    If iDataMode > 0 Then If Trim(OSFLastName.Text) <> '' Then pSaveTDChanges Else pAbortTDChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmTDInfo.pSaveTDChanges;
Begin
  { Turn the TD Info Data Area OFF }
  pTDInfoGB(False);
End;

Procedure TfrmTDInfo.pAbortTDChanges;
Begin
  fdbase.fGetARec(fFilno);
  { Turn the TD Info Data Area OFF }
  pTDInfoGB(False);
  pFillTDFields;
  bEscKeyUsed := False;
  If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
End;

Function TfrmTDInfo.fcheck_tdplayer(Var Player: OpenString): boolean;
Begin
  { iDataMode Integer Variable:  0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  { ((iDataMode = 1) or (iDataMode = 3)) }
  fcheck_tdplayer := false;
  If Not player_check(Player, pnACBL) Then PlayerError := 1
  Else If fduplicate_player(Player, TdInfo_no, 2) Then PlayerError := 2
  Else If ((iDataMode = 1) Or (iDataMode = 3)) And Not fPlayerFound(Player, Playern_no, 2) Then
    PlayerError := 3
  Else fcheck_tdplayer := true;
End;

Function TfrmTDInfo.fcheck_tdnumber(Const tdn: String): boolean;
Begin
  fcheck_tdnumber := Not fKeyFound(tdn, TdInfo_no, 3, true);
End;

Function TfrmTDInfo.fValidEmployeeStatus(Const Field: String): boolean;
Var
  EmpStat: String;
Begin
  EmpStat := Trim(Field);
  fValidEmployeeStatus := false;
  Case Length(EmpStat) Of
    1: fValidEmployeeStatus := Pos(EmpStat[1], 'PFS') > 0;
    2: fValidEmployeeStatus := (Pos(EmpStat, 'EPPMFM') In [1, 3, 5]);
  End;
End;

Function TfrmTDInfo.fPlayerFound(Const number: String; Const Fno, Kno: byte): boolean;
Begin
  fPlayerFound := false;
  If number[7] = ' ' Then Exit;
  fPlayerFound := fKeyFound(Player_key(number), Fno, Kno, false);
End;

Function TfrmTDInfo.fduplicate_player(Const number: String; Const Fno, Kno: byte): boolean;
Begin
  fduplicate_player := false;
  If number[7] = ' ' Then Exit;
  fduplicate_player := fKeyFound(Player_key(number), Fno, Kno, true);
End;

Function TfrmTDInfo.fKeyFound(Const Keytofind: String; Const Fno, Kno: byte;
  Const CheckRecno: boolean): boolean;
Var
  tempkey: keystr;
  trecno: longint;
  found: boolean;
Begin
  { iDataMode Integer Variable:  0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  { ((iDataMode = 1) or (iDataMode = 3)) }
  tempkey := KeytoFind;
  findkey(fIdxKey[Fno, Kno]^, trecno, tempkey);
  fKeyFound := ok;
  If CheckRecno Then fKeyFound := ok And ((trecno <> frecno[Fno])
      Or ((iDataMode = 1) Or (iDataMode = 3)));
  ok := true;
End;

Procedure TfrmTDInfo.ButEditActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { In module Dbase call the procedure PreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  PreEditSaveKeys;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the TD Info Data Area ON }
  pTDInfoGB(True);
  LMDButPlayerInfoDone.Caption := 'Done Editing';
  { Put the cursor in the first field }
  OSFPlayerNo.SetFocus;
End;

Procedure TfrmTDInfo.pTDInfoGB(bEnable: Boolean);
Begin
  pResetFldAttrib;
  { Set Events for Edit Fields }
  //pSetEvents(bEnable);
  { Allow or Deny access to the Player Information fields }
  { Set the state of ActionManager1 for the form players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  GBPlayerInfo.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDButPlayerInfoDone.Enabled := bEnable;
  LMDButPlayerInfoDone.Visible := bEnable;
  // ----------------------------------------------------------------------
  if not bEnable then bEscKeyUsed := False;
End;

Procedure TfrmTDInfo.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  { Turn the TD Info Data Area ON }
  pTDInfoGB(True);
  LMDButPlayerInfoDone.Caption := 'Done Adding';
  //  OSFPlayerNo.Enabled := True;
  //  pOSFieldAvail(True, OSFPlayerNo);
    { Put the cursor in the first field }
  InitRecord(filno);
  pFillTDFields;
  OSFPlayerNo.SetFocus;
End;

Procedure TfrmTDInfo.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  //  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  //  iDataMode := 3;
  //  sDataMode := 'C';
  //  { Turn the TD Info Data Area ON }
  //  pTDInfoGB(True);
  //  LMDButPlayerInfoDone.Caption := 'Done Copying';
  //  { Put the cursor in the first field }
  //  OSFLastName.SetFocus;
End;

Procedure TfrmTDInfo.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  Refresh;
  fFilno := TDInfo_no;
  Case iDataMode Of
    1, 3: Begin
        If Not duplicate_player(OSFPlayerNo.Text, Playern_no, 2, 'C') Then Begin
          pCopyTDRecData;
          fdbase.fAdd_Record;
        End
        Else Begin
          If (MessageDlg('This ACBL Player Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFPlayerNo.SetFocus;
            Exit;
          End
          Else pAbortTDChanges; // Adding or Copying a record
        End;
      End;
    2: Begin
        If fCheckForTDChanges Then Begin
          { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
            in dataset and, if it has, updates the correct field position in dataset
            with the changed data. }
          pCheckTDEditFields;
          frmPlayerLookUp.bKeyIsUnique := PostEditFixKeys;
          If Not frmPlayerLookUp.bKeyIsUnique Then Begin
            If (MessageDlg('This record already exists in this database.', mtError,
              [mbOK], 0) = mrOK) Then Begin
              OSFLastName.SetFocus;
              Exit;
            End
            Else pAbortTDChanges;
          End
          Else fdbase.fPutARec(fFilno);
        End
        Else pAbortTDChanges; // Editing a record
      End;
  End;
  { Turn the Player Info Data Area OFF }
  pTDInfoGB(False);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pUpdateStatusBar;
End;

Procedure TfrmTDInfo.pUpdateStatusBar;
Begin
  iTDReccount := UsedRecs(fDatF[TDInfo_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'TDINFO.DAT') + ' - Contains ' +
    Trim(IntToStr(iTDReccount)) + ' Records.';
End;

Procedure TfrmTDInfo.pCopyTDRecData;
Begin
  { PCOPYTDRECDATA: Saves all data entry fields to their correct position in the dataset.
    The dataset is used by all record saving, adding and copying routines in fDBASE.PAS,
    to update the actual TDInfo.DAT table. }
  TDINFO.LAST_NAME := Pad(OSFLastName.Text,16);
  TDINFO.FIRST_NAME := Pad(OSFFirstName.Text,16);
  TDINFO.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  TDINFO.TD_NO := Pad(OSFTDNumber.Text,3);
  TDINFO.TD_RANK := Pad(OSFTDRank.Text,3);
  TDINFO.EMPL_STAT := Pad(OSFEmployeeStatus.Text,7);
End;

Function TfrmTDInfo.fCheckForTDChanges: Boolean;
Var
  osPlayer_no: OpenString;
Begin
  If OSFPlayerNo.Text <> TDINFO.PLAYER_NO Then Begin
    osPlayer_no := OSFPlayerNo.Text;
    If Not (player_check(osPlayer_no, pnAny)) Then Begin
      MessageDlg('Invalid ACBL Player Number!', mtError, [mbOK], 0);
      Result := True;
    End;
  End;
  If Trim(OSFLastName.Text) <> Trim(TDINFO.LAST_NAME) Then Result := True;
  If Trim(OSFFirstName.Text) <> Trim(TDINFO.FIRST_NAME) Then Result := True;
  If Trim(OSFTDNumber.Text) <> Trim(TDINFO.TD_NO) Then Result := True;
  If Trim(OSFTDRank.Text) <> Trim(TDINFO.TD_RANK) Then Result := True;
  If Trim(OSFEmployeeStatus.Text) <> Trim(TDINFO.EMPL_STAT) Then Result := True;
End;

Procedure TfrmTDInfo.pCheckTDEditFields;
Begin
  { Pad fields with 40 blank spaces, since 40 characters is the widest field length and when
    a record field is stored, extra spaces will be stripped off. }
  TDInfo.LAST_NAME := Pad(OSFLastName.Text,16);
  TDInfo.FIRST_NAME := Pad(OSFFirstName.Text,16);
  TDInfo.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  TDINFO.TD_NO := Pad(OSFTDNumber.Text,3);
  TDINFO.TD_RANK := Pad(OSFTDRank.Text,3);
  TDINFO.EMPL_STAT := Pad(OSFEmployeeStatus.Text,7);
End;

Procedure TfrmTDInfo.ButDeleteActionExecute(Sender: TObject);
Begin
  fFilno := TDInfo_no;
  fDelete_Record(True, frmTDInfo.HelpContext);
  pPlayerNFillFields;
  pFillTDFields;
  pUpdateStatusBar;
  If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
End;

Procedure TfrmTDInfo.pLookForTdRec;
Begin
  // ------------------------------------------------------------
  ksPN := GetKey(Filno, 2);
  ksPNTemp := ksPN;
  // ------------------------------------------------------------
  { Search for records in the player group table that match the Player Number key of the current Player Number record }
  FindKey(fIdxKey[TDInfo_no, 2]^, fRecno[TDInfo_no], ksPNTemp);
  { While the Player Number Key matches, store this players groups to the string grid for viewing }
  If ok And CompareKey(ksPNTemp, ksPN) Then Begin
    GetRec(fDatF[fFilno]^, fRecno[fFilno], Tdinfo);
    fGetARec(fFilno);
  End
  Else
    pFirstTDRecord;
End;

Procedure TfrmTDInfo.Action1Execute(Sender: TObject);
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinNormal := (WindowState = wsNormal);
End;

Procedure TfrmTDInfo.Action2Execute(Sender: TObject);
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinNormal := (WindowState = wsNormal);
End;

Procedure TfrmTDInfo.Action3Execute(Sender: TObject);
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinNormal := (WindowState = wsNormal);
End;

Procedure TfrmTDInfo.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmTDInfo.Action6Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmTDInfo.Action7Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmTDInfo.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
Begin
  If UpperCase(sType) = 'S' Then
    pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
  Else If UpperCase(sType) = 'P' Then
    pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
  Else If UpperCase(sType) = 'C' Then
    pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
  Else If UpperCase(sType) = 'D' Then
    pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
  // -------------------------------------------------------------------------------------------------------
  If bDisplay_Rec Then Begin
    { Load all information to the appropriate field from the current record }
    pFillTDFields;
  End;
  // -------------------------------------------------------------------------------------------------------
  If bWasOnEnter Then Begin
    If UpperCase(sType) = 'S' Then Begin
      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'P') Then Begin
      If Not (oSender As TOvcPictureField).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'D') Then Begin
      If Not (oSender As TOvcDateEdit).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'C') Then Begin
      If Not (oSender As TOvcComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End;
  End;
End;

Procedure TfrmTDInfo.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmTDInfo.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmTDInfo.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmTDInfo.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmTDInfo.pResetFldAttrib;
Var
  iComponent: integer;
Begin
  With Self Do Begin
    For iComponent := 0 To ComponentCount - 1 Do Begin
      If Components[iComponent].ClassType = TOvcSimpleField Then
        pOSFieldAvail(True, (Components[iComponent] As TOvcSimpleField))
      Else If Components[iComponent].ClassType = TOvcPictureField Then
        pOPFieldAvail(True, (Components[iComponent] As TOvcPictureField))
      Else If Components[iComponent].ClassType = TOvcDateEdit Then
        pODEFieldAvail(True, (Components[iComponent] As TOvcDateEdit))
      Else If Components[iComponent].ClassType = TOvcComboBox Then
        pOCBFieldAvail(True, (Components[iComponent] As TOvcComboBox));
    End;
  End;
End;

Procedure TfrmTDInfo.OSFPlayerNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  TDINFO.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  bDoEdit := CustomPLayerEdit(Scrno, 1, TDINFO.PLAYER_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmTDInfo.OSFPlayerNoExit(Sender: TObject);
//Var
//  sPlayerNo: OpenString;
Begin
  //  sPlayerNo := OSFPlayerNo.Text;
  //  If Not (fcheck_tdplayer(sPlayerNo)) Then Begin
  //    MessageDlg('Invalid ACBL Player Number!', mtError, [mbOK], 0);
  //    OSFPlayerNo.SetFocus;
  //  End
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      TDINFO.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
      bDoEdit := CustomPLayerEdit(Scrno, 1, TDINFO.PLAYER_NO, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmTDInfo.OSFLastNameEnter(Sender: TObject);
Begin
  { Used 2 for the field code since it was not assigned in I_Player.Inc }
  bDoEdit := True;
  TDINFO.LAST_NAME := Pad(OSFLastName.Text,16);
  bDoEdit := CustomPLayerEdit(Scrno, 2, TDINFO.LAST_NAME, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmTDInfo.OSFLastNameExit(Sender: TObject);
Begin
  { Used 2 for the field code since it was not assigned in I_Player.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      { Validation of Last Name field when exiting it }
      If trim(OSFLastName.Text) = '' Then Begin
        If MessageDlg('Last Name must NOT be blank! ' + #13 + #10 + '' + #13 + #10 +
          '        Correct this error?', mtError, [mbYes, mbNo], 0) = MRNO Then Begin
          pAbortTDChanges;
        End
        Else OSFLastName.SetFocus
      End
      Else Begin
        TDINFO.LAST_NAME := Pad(OSFLastName.Text,16);
        bDoEdit := CustomPLayerEdit(Scrno, 2, TDINFO.LAST_NAME, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmTDInfo.OSFFirstNameEnter(Sender: TObject);
Begin
  { Used 3 for the field code since it was not assigned in I_Player.Inc }
  bDoEdit := True;
  TDINFO.FIRST_NAME := Pad(OSFFirstName.Text,16);
  bDoEdit := CustomPLayerEdit(Scrno, 3, TDINFO.FIRST_NAME, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmTDInfo.OSFFirstNameExit(Sender: TObject);
Begin
  { Used 3 for the field code since it was not assigned in I_Player.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      TDINFO.FIRST_NAME := Pad(OSFFirstName.Text,16);
      bDoEdit := CustomPLayerEdit(Scrno, 3, TDINFO.FIRST_NAME, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmTDInfo.OSFTDNumberEnter(Sender: TObject);
Begin
  { Used 4 for the field code since it was not assigned in I_Player.Inc }
  bDoEdit := True;
  TDINFO.TD_NO := Pad(OSFTDNumber.Text,3);
  bDoEdit := CustomPLayerEdit(Scrno, 4, TDINFO.TD_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmTDInfo.OSFTDNumberExit(Sender: TObject);
Begin
  { Used 4 for the field code since it was not assigned in I_Player.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (fcheck_tdnumber(OSFTDNumber.Text)) Then Begin
        ErrBox('This TD number already assigned to a different record.', mc, 0);
        OSFTDNumber.Text := '';
        OSFTDNumber.SetFocus;
      End
      Else Begin
        TDINFO.TD_NO := Pad(OSFTDNumber.Text,3);
        bDoEdit := CustomPLayerEdit(Scrno, 4, TDINFO.TD_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmTDInfo.OSFTDRankEnter(Sender: TObject);
Begin
  bDoEdit := True;
  TDINFO.TD_RANK := Pad(OSFTDRank.Text,3);
  bDoEdit := CustomPLayerEdit(Scrno, 6, TDINFO.TD_RANK, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmTDInfo.OSFTDRankExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((OSFTDRank.Text = 'LTD') Or (OSFTDRank.Text = 'ATD') Or (OSFTDRank.Text = 'TD ') Or
        (OSFTDRank.Text = 'AN ') Or (OSFTDRank.Text = 'N  ')) Then Begin
        ErrBox('Invalid TD rank.', mc, 0);
        OSFTDRank.Text := '';
        OSFTDRank.SetFocus;
      End
      Else Begin
        TDINFO.TD_RANK := Pad(OSFTDRank.Text,3);
        bDoEdit := CustomPLayerEdit(Scrno, 6, TDINFO.TD_RANK, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmTDInfo.OSFEmployeeStatusEnter(Sender: TObject);
Begin
  bDoEdit := True;
  TDINFO.EMPL_STAT := Pad(OSFEmployeeStatus.Text,7);
  bDoEdit := CustomPLayerEdit(Scrno, 7, TDINFO.EMPL_STAT, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmTDInfo.OSFEmployeeStatusExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (fValidEmployeeStatus(OSFEmployeeStatus.Text)) Then Begin
        ErrBox('Invalid TD employee status.', mc, 0);
        OSFEmployeeStatus.Text := '';
        OSFEmployeeStatus.SetFocus;
      End
      Else Begin
        TDINFO.EMPL_STAT := Pad(OSFEmployeeStatus.Text,7);
        bDoEdit := CustomPLayerEdit(Scrno, 7, TDINFO.EMPL_STAT, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

function TfrmTDInfo.FormHelp(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  if data < 500000 then WinHelp(Application.Handle,'ACBLSCORE.HLP',Help_Context,Data);
end;

End.

