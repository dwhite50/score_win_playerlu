Unit formPLChange;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, dbSizes, PVIO, VListBox, Clubfees, history, mtables,
  groups,hh;

Type
  { Required for Util2.Player_Check Function }
  OpenString = String[10];
  _DateString = String[8];

Type
  TfrmPLChange = Class(TForm)
    ResizeKit1: TResizeKit;
    OvcController1: TOvcController;
    StaticText1: TStaticText;
    GBPlayerInfo: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Shape1: TShape;
    Label3: TLabel;
    Shape2: TShape;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Shape3: TShape;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Shape4: TShape;
    Shape5: TShape;
    Label15: TLabel;
    Shape6: TShape;
    Label19: TLabel;
    Label20: TLabel;
    Shape7: TShape;
    Shape8: TShape;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Shape13: TShape;
    Label25: TLabel;
    Shape14: TShape;
    Label26: TLabel;
    ButDonePlayerInfo: TButton;
    LMDHLCBState: TLMDHeaderListComboBox;
    LMDHLCBCountry: TLMDHeaderListComboBox;
    LMDHLCBRank: TLMDHeaderListComboBox;
    LMDHLCBMail: TLMDHeaderListComboBox;
    LMDHLCBGender: TLMDHeaderListComboBox;
    LMDHLCBFee: TLMDHeaderListComboBox;
    OSFLastName: TOvcSimpleField;
    OSFFirstName: TOvcSimpleField;
    OSFPlayerNo: TOvcSimpleField;
    OSFStreet1: TOvcSimpleField;
    OSFStreet2: TOvcSimpleField;
    OSFCity: TOvcSimpleField;
    OSFEmail: TOvcSimpleField;
    OPFPaidThru: TOvcPictureField;
    OPFZip: TOvcPictureField;
    OSFUnitNo: TOvcSimpleField;
    OSFDistrictNo: TOvcSimpleField;
    LMDButPlayerInfoDone: TLMDButton;
    OPFCatB: TOvcPictureField;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    GBMasterPoints: TGroupBox;
    Label28: TLabel;
    Shape17: TShape;
    Label29: TLabel;
    Shape18: TShape;
    Label30: TLabel;
    Shape19: TShape;
    Label31: TLabel;
    Shape20: TShape;
    Label32: TLabel;
    OPFPTotal: TOvcPictureField;
    OPFPYTD: TOvcPictureField;
    OPFPMTD: TOvcPictureField;
    OPFPRecent: TOvcPictureField;
    OPFPEligibility: TOvcPictureField;
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF4Key: TButton;
    ButtonF7Key: TButton;
    StaticTextF4Key: TStaticText;
    StaticTextF7Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    StatusBar1: TStatusBar;
    LMDHint1: TLMDHint;
    Label16: TLabel;
    Button1: TButton;
    ShiftF4: TAction;
    StaticText2: TStaticText;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    OPFPhone: TOvcPictureField;
    OPFLocalActiveDate: TOvcPictureField;
    OPFAcblActiveDate: TOvcPictureField;
    OPFLastACBLUpdate: TOvcPictureField;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure CtrlCKeyExecute(Sender: TObject);
    Procedure pFillPLUFields;
    Procedure pNextPNRecord;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure pPrevPNRecord;
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure pFirstPNRecord;
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pGBMasterPoints(bEnable: Boolean);
    Procedure PrintPlayer;
    Procedure F7KeyExecute(Sender: TObject);
    Procedure MasterPointsKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pMasterPointsDone;
    Procedure pSaveMPChanges;
    Procedure pAbortMPChanges;
    Procedure OPFPEligibilityExit(Sender: TObject);
    Procedure pCalcPoints(Sender: TObject);
    Procedure pGBPlayerInfoReadOnly(bLogical: Boolean);
    Procedure pGBMasterPointsReadOnly(bLogical: Boolean);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure OSFLastNameExit(Sender: TObject);
    Procedure OSFPlayerNoExit(Sender: TObject);
    Procedure ComboBoxChange(Sender: TObject);
    Procedure ComboBoxEnter(Sender: TObject);
    Function fFindCBItemIndex(cbName: TObject; sText2Find: String): Integer;
    Procedure LMDHLCBEnter(Sender: TObject);
    Procedure OSFDistrictNoExit(Sender: TObject);
    Procedure pDateExit(Sender: TObject);
    Procedure LMDHLCBRankExit(Sender: TObject);
    Procedure LMDHLCBMailExit(Sender: TObject);
    Procedure LMDHLCBGenderExit(Sender: TObject);
    Procedure LMDHLCBFeeExit(Sender: TObject);
    Procedure FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure FormKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure MailingAddressCheck;
    Procedure OSFUnitNoExit(Sender: TObject);
    Procedure pAbortChanges;
    Function Status_OK(Const Fno: Integer): Boolean;
    Function fGetLength(iFileNo: Integer; sFieldName: String): Integer;
    Procedure pCheckEditFields;
    Function fCheckForChanges: Boolean;
//    Procedure pCopyRecData;
    Procedure pSaveChanges;
    Procedure pPlayerInfo(bEnable: Boolean);
    Procedure pUpdateStatusBar;
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure FlushAFile(Const Fno: Integer);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure pSetEvents(bSwitchEventsOn: Boolean);
    Procedure OPFPaidThruExit(Sender: TObject);
    Procedure F3KeyExecute(Sender: TObject);
    Procedure F4KeyExecute(Sender: TObject);
    Procedure ShiftF4Execute(Sender: TObject);
    Procedure OpenFiles;
    Procedure CloseFiles;
    Procedure FormShow(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Procedure pDateFieldKeyPress(Sender: TObject; Var Key: Char);
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure Action3Execute(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    Procedure pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
    Procedure pResetFldAttrib;
    Procedure OSFLastNameEnter(Sender: TObject);
    Procedure OSFFirstNameEnter(Sender: TObject);
    Procedure OSFFirstNameExit(Sender: TObject);
    Procedure OSFPlayerNoEnter(Sender: TObject);
    Procedure OSFStreet1Enter(Sender: TObject);
    Procedure OSFStreet1Exit(Sender: TObject);
    Procedure OSFStreet2Enter(Sender: TObject);
    Procedure OSFStreet2Exit(Sender: TObject);
    Procedure OSFCityEnter(Sender: TObject);
    Procedure OSFCityExit(Sender: TObject);
    Procedure LMDHLCBStateEnter(Sender: TObject);
    Procedure LMDHLCBStateExit(Sender: TObject);
    Procedure OPFZipEnter(Sender: TObject);
    Procedure OPFZipExit(Sender: TObject);
    Procedure LMDHLCBCountryEnter(Sender: TObject);
    Procedure LMDHLCBCountryExit(Sender: TObject);
    Procedure OSFEmailEnter(Sender: TObject);
    Procedure OSFEmailExit(Sender: TObject);
    Procedure LMDHLCBRankEnter(Sender: TObject);
    Procedure OPFCatBEnter(Sender: TObject);
    Procedure OPFCatBExit(Sender: TObject);
    Procedure LMDHLCBMailEnter(Sender: TObject);
    Procedure OSFUnitNoEnter(Sender: TObject);
    Procedure OSFDistrictNoEnter(Sender: TObject);
    Procedure LMDHLCBGenderEnter(Sender: TObject);
    Procedure LMDHLCBFeeEnter(Sender: TObject);
    Procedure OPFPaidThruEnter(Sender: TObject);
    Procedure ButtonF1KeyClick(Sender: TObject);
    Procedure OPFPhoneEnter(Sender: TObject);
    Procedure OPFPhoneExit(Sender: TObject);
    function FormHelp(Command: Word; Data: Integer;
      var CallHelp: Boolean): Boolean;
  private
    { Private declarations }
  public
    { Public declarations }
    { Integer variable used dynamically to store the tables record count }
    iReccount: Integer;
    sStandFuncKeyHint: String;
    { iDataMode Integer Variable
      Set the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
//    iDataMode: Integer;
  End;

Var
  frmPLChange: TfrmPLChange;
  ksPLChange, ksPLChangeTemp: keystr;
  ksFirstKey: KeyStr;
  aOK: Boolean;
  iPosChar: Integer;
  bOffice: Boolean;
  bCompare: Boolean;
  bRecCount: Boolean;

  {$I fdbnames.dcl}

Function duplicate_player(Const number: String; Fno, Kno: integer; Func: String): boolean;

Implementation

Uses formTDInfo, formPlayerLookUp;

{$DEFINE indatabase}
{$DEFINE isnoattend}
{$I STSTRS.dcl}
{$I showpts.inc}
{$I addform.inc}
{$I fixdb.inc}
{$I i_player.inc}

Procedure PleaseWait;
Begin
  PlsWait;
End;
{$R *.dfm}

Procedure TfrmPLChange.ButQuitActionExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmPLChange.FormActivate(Sender: TObject);
Begin
  { Must be set to true for Win32 applications. Used in Dbase and Fdbase }
  UseDBLinkages := True;
  UseLookupDB := True;
  Application.HintHidePause := 200000;
  sStandFuncKeyHint := 'Use this Function Key to ';
  //==========================================================
  { These variables are declared in CEDef.pas }
  IniName := GetCurrentDir + '\' + DefIniName;
  AppName := 'PLChange';
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is made to a "Record",
    dbase re-writes the table header and in Playern.dat, updates dbLastImpDate entry. Set them to
    False when you Delete a record, then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  Scrno := 1;
  //==========================================================
  CSDef.ReadCFG;
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ============================================================================
  { If we are using the player file at the office, it will be on drive E:
    PLPath Contains the Player Lookup Path information }
  Prepend := FixPath(CFG.PLPath);
  iPosChar := pos('E:\', UpperCase(Prepend));
  If iPosChar > 0 Then Begin
    { We ARE using ACBLscore at the ACBL Office Site }
    {$DEFINE Office};
    bOffice := True;
    ButEdit.Enabled := False;
    ButEditAction.Enabled := False;
    ButAdd.Enabled := False;
    ButAddAction.Enabled := False;
    ButCopy.Enabled := False;
    ButCopyAction.Enabled := False;
    ButDelete.Enabled := False;
    ButDeleteAction.Enabled := False;
  End
  Else bOffice := False; { We ARE NOT using ACBLscore at the ACBL Office Site }
  // ============================================================================

  // ----------------------------------------------------------------------------------------------------------
  // -----------------------------------------
  { Store the number of records in PLChange.DAT to iReccount }
  iReccount := db33.UsedRecs(DatF[PLChange_no]^);
  If iReccount < 0 Then iReccount := 0;
  dBase.Filno := PLChange_no;
  // -----------------------------------------
  ClearKey(IdxKey[PLChange_no, 1]^);
  bRecCount := (iReccount >= 1);
  If bRecCount Then Begin
    { Find the first available record and load it's values to the fields on the screen }
    pFirstPNRecord;
  End
  Else Begin
    // If there are no Player Records
    Close;
  End;
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(fPrepend + 'PLChange.DAT');
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iReccount)) + ' Records.';
  { Set Events for Edit Fields }
  pSetEvents(False);
  If bWinNormal Then WindowState := wsNormal
  Else WindowState := wsMaximized;
End;

Procedure TfrmPLChange.CtrlCKeyExecute(Sender: TObject);
Var
  wcbClipText: tClipboard;
Begin
  wcbClipText := tClipboard.Create;
  wcbClipText.Clear;
  If Not Empty(OSFEmail.Text) Then Begin
    wcbClipText.AsText := ('"' + Trim(OSFFirstName.Text) + ' ' + Trim(OSFLastName.Text) + '" <'
      + Trim(OSFEmail.Text) + '>');
    ErrBox('Email address has been copied to Windows Clipboard', MC, 0)
  End
  Else ErrBox('Email address is empty', MC, 0);
  wcbClipText.Free;
End;

Procedure TfrmPLChange.pFillPLUFields;
Var
  sEmail: String;
  rPoints: Extended;
Begin
  // -----------------------------------------------------------
  { The field CatA is not used in the Player Lookup Module! }
  // -----------------------------------------------------------
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  OSFLastName.Text := Trim(PLChange.LAST_NAME);
  OSFFirstName.Text := Trim(PLChange.FIRST_NAME);
  OSFPlayerNo.Text := Trim(PLChange.PLAYER_NO);
  OSFStreet1.Text := Trim(PLChange.STREET1);
  OSFStreet2.Text := Trim(PLChange.STREET2);
  OSFCity.Text := Trim(PLChange.CITY);
  // STATE   ----------------------------------------------------------------------
  LMDHLCBState.Text := Trim(PLChange.STATE);
  // ------------------------------------------------------------------------------
  OPFZip.Text := Trim(PLChange.ZIP);
  // COUNTRY ----------------------------------------------------------------------
  LMDHLCBCountry.Text := Trim(PLChange.COUNTRY);
  // ------------------------------------------------------------------------------
  sEmail := Trim(PLChange.EMAIL);
  OSFEmail.Text := Trim(PLChange.EMAIL);
  // ------------------------------------------------------------------------------
  OPFPhone.Text := Trim(PLChange.PHONE);
  // ------------------------------------------------------------------------------
  OPFLocalActiveDate.Text := PLChange.LOCAL_DATE;
  OPFAcblActiveDate.Text := PLChange.ACBL_DATE;
  // ------------------------------------------------------------------------------
  LMDHLCBRank.Text := PLChange.ACBL_RANK;
  // ------------------------------------------------------------------------------
  OPFCatB.Text := PLChange.CAT_B;
  // ------------------------------------------------------------------------------
  LMDHLCBMail.Text := PLChange.MAIL_CODE;
  // ------------------------------------------------------------------------------
  OSFUnitNo.Text := Trim(PLChange.UNIT_NO);
  OSFDistrictNo.Text := Trim(PLChange.DISTRICT_NO);
  // ------------------------------------------------------------------------------
  LMDHLCBGender.Text := PLChange.GENDER;
  LMDHLCBFee.Text := PLChange.FEE_TYPE;
  // ------------------------------------------------------------------------------
  OPFPaidThru.Text := PLChange.PAID_THRU;
  // ------------------------------------------------------------------------------
  OPFLastACBLUpdate.Text := PLChange.ACBL_UPDATE_DATE;
  // ==============================================================================================
  { Calculate the Players Total Points }
  OPFPTotal.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppTotal) * 0.010,
    ffFixed, 15, 2);
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points YTD }
  OPFPYTD.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppYTD) * 0.010,
    ffFixed, 15, 2);
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points MTD }
  OPFPMTD.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppMTD) * 0.010,
    ffFixed, 15, 2);
  // ------------------------------------------------------------------------------------
  { Calculate the Players Recent Points }
  OPFPRecent.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppRecent) * 0.010,
    ffFixed, 15, 2);
  // ------------------------------------------------------------------------------------
  { Calculate the Players Eligibility Points }
  OPFPEligibility.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppElig) * 0.010,
    ffFixed, 15, 2);
End;

Procedure TfrmPLChange.ButNextActionExecute(Sender: TObject);
Begin
  pNextPNRecord; // Move the record pointer in the table to the next available record and load the data in the screen fields
  If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
End;

Procedure TfrmPLChange.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevPNRecord; // Move the record pointer in the table to the previous record and load the data in the screen fields
  If ButPrev.Enabled Then ButPrev.SetFocus Else ButQuit.SetFocus;
End;

Procedure TfrmPLChange.pPrevPNRecord;
Begin
  { ksPLChange, ksPLChangeTemp: keystr }
  { Move the record pointer in the table to the previous record and load the data in the screen
    fields }
//  PrevKey(IdxKey[PLChange_no, 1]^, Recno[PLChange_no], ksPLChangeTemp);
//  If OK Then Begin
//    GetARec(PLChange_no); // Store the Record
//    pFillPLUFields;
//  End
//  Else pNextPNRecord;
  Prev_Record;
  pFillPLUFields;
End;

Procedure TfrmPLChange.pNextPNRecord;
Begin
  { ksPLChange, ksPLChangeTemp: keystr }
//  NextKey(IdxKey[PLChange_no, 1]^, Recno[PLChange_no], ksPLChangeTemp);
//  If OK Then Begin
//    GetARec(PLChange_no); // Store the Record
//    pFillPLUFields;
//  End
//  Else Begin
//    { Move the record pointer in the table to the previous record and load the data in the screen
//      fields }
//    pPrevPNRecord;
//  End;
  Next_Record;
  pFillPLUFields;
End;

Procedure TfrmPLChange.ButTopActionExecute(Sender: TObject);
Begin
  pFirstPNRecord;
End;

Procedure TfrmPLChange.pFirstPNRecord;
Begin
  { Find the first available record and load it's values to the fields on the screen }
//  ClearKey(IdxKey[PLChange_no, 1]^);
//  LibData.SkipNext(PLChange_no, ksFirstKey);
//  GetARec(PLChange_no);
//  // ------------------------------------------------------
//  { ksPLChange, ksPLChangeTemp: keystr }
//  ksPLChange := GetKey(PLChange_no, 1);
//  ksPLChangeTemp := ksPLChange;
//  pFillPLUFields;
  Top_Record;
  pFillPLUFields;
End;

Procedure TfrmPLChange.ButLastActionExecute(Sender: TObject);
Begin
  //  ClearKey(IdxKey[PLChange_no, 1]^);
  //  PrevKey(IdxKey[PLChange_no, 1]^, Recno[PLChange_no], ksPLChangeTemp);
  //  GetARec(PLChange_no);
  //  // ------------------------------------------------------
  //  { ksPLChange, ksPLChangeTemp: keystr }
  //  ksPLChange := GetKey(PLChange_no, 1);
  //  ksPLChangeTemp := ksPLChange;
  //  pFillPLUFields;
  Last_Record;
  pFillPLUFields;
End;

Procedure TfrmPLChange.ButFindActionExecute(Sender: TObject);
Begin
  // --------------------------------------------------
  frmPLChange.Enabled := False;
  ActionManager1.State := asSuspended;
  // --------------------------------------------------
  pGenericFind;
  // --------------------------------------------------
  frmPLChange.Enabled := True;
  ActionManager1.State := asNormal;
  // --------------------------------------------------
  GetARec(PLChange_no); // Store the Attendance Record
  // ------------------------------------------------------
  { ksPLChange, ksPLChangeTemp: keystr }
  ksPLChange := GetKey(PLChange_no, 1);
  ksPLChangeTemp := ksPLChange;
  pFillPLUFields;
End;

Procedure TfrmPLChange.F2KeyExecute(Sender: TObject);
Begin
  { Enable the Group Box for Master Points for Editing }
  pGBMasterPoints(True);
End;

Procedure TfrmPLChange.pGBMasterPoints(bEnable: Boolean);
Begin
  { Turn the Player Info Data Area ON }
  { Allow or Deny access to the Player Information fields }
  { Set the state of ActionManager1 for the form players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  GBMasterPoints.Enabled := bEnable;
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 2;
  sDataMode := 'E';
  OPFPTotal.SetFocus;
End;

Procedure TfrmPLChange.PrintPlayer;
Var
  today: String[6];
  isrenewal: boolean;
  esc: boolean;
Begin
  esc := false;
  Today := Copy(sysdate, 5, 4) + Copy(sysdate, 1, 2);
  With Playern Do Begin
    isrenewal := Player_Check(Player_no, pnACBL)
      And ((Copy(paid_thru, 3, 4) + Copy(paid_thru, 1, 2) <= Today) Or YesNoBox(esc, false,
      'Include membership renewal form', 0, MC, Nil));
  End;
  If esc Then exit;
  PrintAddressForm(true, isrenewal);
End;

Procedure TfrmPLChange.F7KeyExecute(Sender: TObject);
Begin
  PrintPlayer;
End;

Procedure TfrmPLChange.MasterPointsKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  //If key = VK_RETURN Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  If key = VK_RETURN Then Begin
    key := 0;
    keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortMPChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    If iDataMode > 0 Then pSaveMPChanges;
  End;
End;

Procedure TfrmPLChange.pAbortMPChanges;
Begin
  { Read data base file PLChange_no.  Record number in Recno[PLChange_no]
    Reload the record before any changes where made. }
  GetARec(PLChange_no);
  { Calculate the Players Total Points }
  OPFPTotal.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppTotal) * 0.010,
    ffFixed, 15, 2);
  { Calculate the Players Points YTD }
  OPFPYTD.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppYTD) * 0.010,
    ffFixed, 15, 2);
  { Calculate the Players Points MTD }
  OPFPMTD.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppMTD) * 0.010,
    ffFixed, 15, 2);
  { Calculate the Players Recent Points }
  OPFPRecent.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppRecent) * 0.010,
    ffFixed, 15, 2);
  { Calculate the Players Eligibility Points }
  OPFPEligibility.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppElig) * 0.010,
    ffFixed, 15, 2);
  pMasterPointsDone;
End;

Procedure TfrmPLChange.pSaveMPChanges;
Begin
  { Save the Players Total Points }
  dBase.SetPlayerPoints(Round(Valu(OPFPTotal.Text) * 100), ppTotal, False);
  { Save the Players Points YTD }
  dBase.SetPlayerPoints(Round(Valu(OPFPYTD.Text) * 100), ppYTD, False);
  { Save the Players Points MTD }
  dBase.SetPlayerPoints(Round(Valu(OPFPMTD.Text) * 100), ppMTD, False);
  { Save the Players Recent Points }
  dBase.SetPlayerPoints(Round(Valu(OPFPRecent.Text) * 100), ppRecent, False);
  { Save the Players Eligibility Points }
  dBase.SetPlayerPoints(Round(Valu(OPFPEligibility.Text) * 100), ppElig, False);
  { Save the Changes back to the Database }
  dbase.PutARec(PLChange_no);
  { Set the form back to normal view mode }
  pMasterPointsDone;
End;

Procedure TfrmPLChange.pMasterPointsDone;
Begin
  { Make the GroupBox GBMasterPoints read only, all Maskedits are in this GB, so they by
    default, are read only also }
  pGBMasterPointsReadOnly(True);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  ActionManager1.State := asNormal;
  // ----------------------------------------------------------------------
  GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := True;
  GBMenu.Align := alBottom;
  bEscKeyUsed := False;
  ButNext.SetFocus;
End;

Procedure TfrmPLChange.pCalcPoints(Sender: TObject);
Begin
  // ==============================================================================================
  { Total Master Points }
  If (Sender As TOvcPictureField).Name = 'OPFPTotal' Then Begin
    { Save the Players Total Points }
    dBase.SetPlayerPoints(Round(Valu(OPFPTotal.Text) * 100), ppTotal, False);
    { Calculate the Players Total Points }
    OPFPTotal.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppTotal) * 0.010,
      ffFixed, 15, 2);
  End;
  // ==============================================================================================
  { YTD Master Points }
  If (Sender As TOvcPictureField).Name = 'OPFPYTD' Then Begin
    { Save the Players Points YTD }
    dBase.SetPlayerPoints(Round(Valu(OPFPYTD.Text) * 100), ppYTD, False);
    { Calculate the Players Points YTD }
    OPFPYTD.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppYTD) * 0.010,
      ffFixed, 15, 2);
  End;
  // ==============================================================================================
  { MTD Master Points }
  If (Sender As TOvcPictureField).Name = 'OPFPMTD' Then Begin
    { Save the Players Points MTD }
    dBase.SetPlayerPoints(Round(Valu(OPFPMTD.Text) * 100), ppMTD, False);
    { Calculate the Players Points MTD }
    OPFPMTD.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppMTD) * 0.010,
      ffFixed, 15, 2);
  End;
  // ==============================================================================================
  { Recent Master Points }
  If (Sender As TOvcPictureField).Name = 'OPFPRecent' Then Begin
    { Save the Players Recent Points }
    dBase.SetPlayerPoints(Round(Valu(OPFPRecent.Text) * 100), ppRecent, False);
    { Calculate the Players Recent Points }
    OPFPRecent.Text := FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppRecent) * 0.010,
      ffFixed, 15, 2);
  End;
  // ==============================================================================================
  { If the RECENT Master Points is greater than MTD Master Points }
  If (OPFPRecent.Text > OPFPMTD.Text) Then
    OPFPMTD.Text := OPFPRecent.Text;
  // ==============================================================================================
  { If the MTD Master Points is greater than YTD Master Points }
  If (OPFPMTD.Text > OPFPYTD.Text) Then
    OPFPYTD.Text := OPFPMTD.Text;
  // ==============================================================================================
  { If the YTD Master Points is greater than TOTAL Master Points }
  If (OPFPYTD.Text > OPFPTotal.Text) Then
    OPFPTotal.Text := OPFPYTD.Text;
End;

Procedure TfrmPLChange.OPFPEligibilityExit(Sender: TObject);
Begin
  { Save changes to the Master Points }
  pSaveMPChanges;
End;

Procedure TfrmPLChange.pGBPlayerInfoReadOnly(bLogical: Boolean);
Begin
  { pGBPlayerInfoReadOnly: Sets the enabled property of GBPlayerInfo to the boolean variable passed }
  GBPlayerInfo.Enabled := Not bLogical;
End;

Procedure TfrmPLChange.pGBMasterPointsReadOnly(bLogical: Boolean);
Begin
  { pGBMasterPointsReadOnly: Sets the enabled property of GBMasterPoints to the boolean variable passed }
  GBMasterPoints.Enabled := Not bLogical;
End;

Procedure TfrmPLChange.F1KeyExecute(Sender: TObject);
Begin
  //pRunExtProg(frmPLChange, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmPLChange.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = VK_RETURN Then Begin
    key := 0;
    If iDataMode > 0 Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    If iDataMode > 0 Then pAbortMPChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    { At least the Last Name Field must be filled in... }
    If iDataMode > 0 Then If Trim(OSFLastName.Text) <> '' Then pSaveChanges Else pAbortChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmPLChange.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    { At least the Last Name Field must be filled in... }
    If iDataMode > 0 Then If Trim(OSFLastName.Text) <> '' Then pSaveChanges Else pAbortChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmPLChange.ComboBoxChange(Sender: TObject);
Var
  iForLoop, iPos, iTxtLen: Integer;
  sTempStr: String;
Begin
  { COMBOBOXCHANGE: TLMDHeaderListComboBox OnChange Routine }
  If (Sender As TLMDHeaderListComboBox).Focused Then Begin
    With (Sender As TLMDHeaderListComboBox) Do Begin
      sTempStr := Text;
      For iForLoop := 0 To (ListBox.Items.Count - 1) Do Begin
        iPos := Pos(sTempStr, ListBox.Items[iForLoop]);
        iTxtLen := Length(sTempStr);
        If (iPos = 1) Or (iPos = 2) Then Begin
          ListBox.ItemIndex := iForLoop;
          Break;
        End;
      End;
    End;
  End;
End;

Procedure TfrmPLChange.ComboBoxEnter(Sender: TObject);
Begin
  { COMBOBOXENTER: TLMDHeaderListComboBox OnEnter Routine }
  With (Sender As tLMDHeaderListComboBox) Do Begin
    DroppedDown := True;
    ListBox.AlternateColors := True;
    Refresh;
    If ListBox.ItemIndex <> -1 Then ListBox.ItemIndex := fFindCBItemIndex(Sender, Text)
    Else ListBox.ItemIndex := 1;
  End;
End;

Function TfrmPLChange.fFindCBItemIndex(cbName: TObject; sText2Find: String): Integer;
Var
  iForLoop: Integer;
  sTempStr: String;
Begin
  { FFINDCBITEMINDEX: Used to see if there is a matching entry in the current
    TLMDHeaderListComboBox object. Returns the index position if there is or 0 if not. }
  Result := 1;
  With (cbName As TLMDHeaderListComboBox) Do Begin
    sTempStr := Text;
    For iForLoop := 0 To (ListBox.Items.Count - 1) Do Begin
      If Pos(sTempStr, ListBox.Items[iForLoop]) = 1 Then Begin
        Result := iForLoop;
        Break;
      End;
    End;
  End;
End;

Function duplicate_player(Const number: String; Fno, Kno: integer; Func: String): boolean;
Var
  tempkey: keystr;
  trecno: longint;
  Pn: String[7];
Begin
  duplicate_player := false;
  pn := number;
  If Not player_check(pn, pnPoundACBL) Then exit;
  tempkey := Pads(player_key(number), 7);
  findkey(IdxKey[Fno, Kno]^, trecno, tempkey);
  If ok
    And ((trecno <> recno[PLChange_no]) Or ((func = 'A') Or (func = 'C'))) Then
    duplicate_player := true;
  ok := true;
End;

Procedure TfrmPLChange.LMDHLCBEnter(Sender: TObject);
Begin
  { TLMDHLCBCountry OnEnter Routine }
  With (Sender As tLMDHeaderListComboBox) Do Begin
    ListBox.AlternateColors := True;
  End;
End;

Procedure TfrmPLChange.pDateExit(Sender: TObject);
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
End;

Procedure TfrmPLChange.FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End;
End;

Procedure TfrmPLChange.FormKeyUp(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End;
End;

Procedure TfrmPLChange.MailingAddressCheck;
Var
  bGoodMailingAddress: Boolean;
Begin
  bGoodMailingAddress := True;
  If Trim(OSFStreet1.Text) = '' Then bGoodMailingAddress := False;
  If Trim(OSFCity.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(LMDHLCBState.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(LMDHLCBState.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(OPFZip.Text) <> '' Then bGoodMailingAddress := False;
  If bGoodMailingAddress Then Begin
    If Trim(LMDHLCBMail.Text) = '' Then LMDHLCBMail.Text := 'M';
  End;
End;

Procedure TfrmPLChange.pAbortChanges;
Begin
  { Set Events for Edit Fields }
  pSetEvents(False);
  { Turn the Player Info Data Area OFF }
  pPlayerInfo(False);
  //-------------------------------------------------------------------------------------
  If iDataMode = 1 Then Begin // If adding a record
    ClearKey(IdxKey[PLChange_no, 1]^);
    { Find the first available record and load it's values to the fields on the screen }
    pNextPNRecord;
  End
  Else
    { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
    pFillPLUFields;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  bEscKeyUsed := False;
  If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
End;

Function TfrmPLChange.Status_OK(Const Fno: Integer): Boolean;
Var
  TOK: Boolean;
Begin
  TOK := True;
  TOK := (UsedRecs(DatF[Fno]^) > 0);
  Status_OK := TOK
End;

Function TfrmPLChange.fGetLength(iFileNo: Integer; sFieldName: String): Integer;
Begin
  Result := dbSizes.PLChange_no
End;

Procedure TfrmPLChange.pCheckEditFields;
Begin
  // -----------------------------------------------------------
  { The field CatA is not used in the Player Lookup Module! }
  // -----------------------------------------------------------
  { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
    in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
    with the changed data. The PLAYERN dataset is used by all record saving, adding and copying
    routines in DBASE.PAS, to update the actual PLChange.DAT table.}
  { Pad fields with 40 blank spaces, since 40 characters is the widest field length and when
  a record field is stored, extra spaces will be stripped off. }
  PLChange.LAST_NAME := Pad(OSFLastName.Text,16);
  PLChange.FIRST_NAME := Pad(OSFFirstName.Text,16);
  PLChange.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  PLChange.STREET1 := Pad(OSFStreet1.Text,30);
  PLChange.STREET2 := Pad(OSFStreet2.Text,26);
  PLChange.CITY := Pad(OSFCity.Text,16);
  PLChange.STATE := Pad(LMDHLCBState.Text,2);
  PLChange.ZIP := Pad(OPFZip.Text,10);
  PLChange.COUNTRY := Pad(LMDHLCBCountry.Text,2);
  PLChange.EMAIL := Pad(OSFEmail.Text,40);
  PLChange.PHONE := Pad(OPFPhone.GetStrippedEditString,20);
  PLChange.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
  PLChange.CAT_B := Pad(OPFCatB.Text,1);
  PLChange.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
  PLChange.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
  PLChange.DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
  PLChange.GENDER := Pad(LMDHLCBGender.Text,1);
  PLChange.FEE_TYPE := Pad(LMDHLCBFee.Text,1);
  PLChange.PAID_THRU := Pad(OPFPaidThru.GetStrippedEditString,6);
End;

Function TfrmPLChange.fCheckForChanges: Boolean;
Var
  osPlayer_no: OpenString;
Begin
  Result := false;
  // -----------------------------------------------------------
  { The field CatA is not used in the Player Lookup Module! }
  // -----------------------------------------------------------
  { Check to see if the player number has changed, then check to make sure it is not a duplicate
    player number. }
  If trim(OSFPlayerNo.Text) <> trim(PLChange.PLAYER_NO) Then Begin
    osPlayer_no := OSFPlayerNo.Text;
    If Not (player_check(osPlayer_no, pnAny)) Then Begin
      MessageDlg('Invalid ACBL Player Number!', mtError, [mbOK], 0);
      Result := True;
    End;
  End;

  If Trim(OSFLastName.Text) <> Trim(PLChange.LAST_NAME) Then Result := True;
  If Trim(OSFFirstName.Text) <> Trim(PLChange.FIRST_NAME) Then Result := True;
  If Trim(OSFStreet1.Text) <> Trim(PLChange.STREET1) Then Result := True;
  If Trim(OSFStreet2.Text) <> Trim(PLChange.STREET2) Then Result := True;
  If Trim(OSFCity.Text) <> Trim(PLChange.CITY) Then Result := True;
  If Trim(LMDHLCBState.Text) <> Trim(PLChange.STATE) Then Result := True;
  If Trim(OPFZip.Text) <> Trim(PLChange.ZIP) Then Result := True;
  If Trim(LMDHLCBCountry.Text) <> Trim(PLChange.COUNTRY) Then Result := True;
  If Trim(OSFEmail.Text) <> Trim(PLChange.EMAIL) Then Result := True;
  If Trim(OPFPhone.GetStrippedEditString) <> Trim(PLChange.PHONE) Then Result := True;
  If Trim(LMDHLCBRank.Text) <> Trim(PLChange.ACBL_RANK) Then Result := True;
  If Trim(OPFCatB.Text) <> Trim(PLChange.CAT_B) Then Result := True;
  If Trim(LMDHLCBMail.Text) <> Trim(PLChange.MAIL_CODE) Then Result := True;
  If Trim(OSFUnitNo.Text) <> Trim(PLChange.UNIT_NO) Then Result := True;
  If Trim(OSFDistrictNo.Text) <> Trim(PLChange.DISTRICT_NO) Then Result := True;
  If Trim(LMDHLCBGender.Text) <> Trim(PLChange.GENDER) Then Result := True;
  If Trim(LMDHLCBFee.Text) <> Trim(PLChange.FEE_TYPE) Then Result := True;
  If Trim(OPFPaidThru.GetStrippedEditString) <> Trim(PLChange.PAID_THRU) Then Result := True;
End;

{Procedure TfrmPLChange.pCopyRecData;
Begin
  // -----------------------------------------------------------
  { The field CatA is not used in the Player Lookup Module! }
  // -----------------------------------------------------------
  { PCOPYRECDATA: Saves all data entry fields to their correct position in the PLAYERN dataset.
    The PLAYERN dataset is used by all record saving, adding and copying routines in DBASE.PAS,
    to update the actual PLChange.DAT table. }
 { PLChange.LAST_NAME := fPadRight(OSFLastName.Text, 40);
  PLChange.FIRST_NAME := fPadRight(OSFFirstName.Text, 40);
  PLChange.PLAYER_NO := fPadRight(OSFPlayerNo.Text, 40);
  PLChange.STREET1 := fPadRight(OSFStreet1.Text, 40);
  PLChange.STREET2 := fPadRight(OSFStreet2.Text, 40);
  PLChange.CITY := fPadRight(OSFCity.Text, 40);
  PLChange.STATE := fPadRight(LMDHLCBState.Text, 40);
  PLChange.ZIP := fPadRight(OPFZip.Text, 40);
  PLChange.COUNTRY := fPadRight(LMDHLCBCountry.Text, 40);
  PLChange.EMAIL := fPadRight(OSFEmail.Text, 40);
  { Both Phone Numbers are stored in one field, Phone Number 1 first 10 characters,
    Phone Number 2 second 10 characters}
  {PLChange.PHONE := fPadRight(OPFPhone.GetStrippedEditString, 40);
  PLChange.ACBL_RANK := fPadRight(LMDHLCBRank.Text, 40);
  PLChange.CAT_B := fPadRight(OPFCatB.Text, 40);
  PLChange.MAIL_CODE := fPadRight(LMDHLCBMail.Text, 40);
  PLChange.UNIT_NO := fPadRight(OSFUnitNo.Text, 40);
  PLChange.DISTRICT_NO := fPadRight(OSFDistrictNo.Text, 40);
  PLChange.GENDER := fPadRight(LMDHLCBGender.Text, 40);
  PLChange.FEE_TYPE := fPadRight(LMDHLCBFee.Text, 40);
  PLChange.PAID_THRU := fPadRight(OPFPaidThru.GetStrippedEditString, 40);
End;}

Procedure TfrmPLChange.pSaveChanges;
Begin
  { Set Events for Edit Fields }
  pSetEvents(False);
  Refresh;
  dbase.Filno := PLChange_no;
  Case iDataMode Of
    1, 3: Begin
        If Not duplicate_player(OSFPlayerNo.Text, PLChange_no, 2, 'C') Then Begin
          MailingAddressCheck;
          pCheckEditFields;
          dbase.Add_Record;
        End
        Else Begin
          If (MessageDlg('This ACBL Player Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFPlayerNo.SetFocus;
            Exit;
          End
          Else pAbortChanges; // Adding or Copying a record
        End;
      End;
    2: Begin
        If fCheckForChanges Then Begin
          { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
            in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
            with the changed data. }
          MailingAddressCheck;
          pCheckEditFields;
          dbase.PutARec(PLChange_no);
        End
        Else pAbortChanges; // Editing a record
      End;
  End;
  // Turn the Player Info Data Area OFF
  pPlayerInfo(False);
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 0;
  sDataMode := null;
  pUpdateStatusBar;
End;

Procedure TfrmPLChange.pPlayerInfo(bEnable: Boolean);
Begin
  pResetFldAttrib;
  { Set Events for Edit Fields }
  pSetEvents(bEnable);
  { Allow or Deny access to the Player Information fields }
  { Set the state of ActionManager1 for the form players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  GBPlayerInfo.Enabled := bEnable;
  // ----------------------------------------------------------------------
  GBMasterPoints.Visible := Not bEnable;
  // ----------------------------------------------------------------------
  LMDButPlayerInfoDone.Enabled := bEnable;
  LMDButPlayerInfoDone.Visible := bEnable;
  // ----------------------------------------------------------------------
  If Not bEnable Then bEscKeyUsed := False;
End;

Procedure TfrmPLChange.pUpdateStatusBar;
Begin
  iReccount := UsedRecs(DatF[PLChange_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'PLChange.DAT') + ' - Contains ' +
    Trim(IntToStr(iReccount)) + ' Records.';
End;

Procedure TfrmPLChange.ButDeleteActionExecute(Sender: TObject);
Begin
  dbase.Filno := PLChange_no;
  dbase.Delete_Record(True, frmPLChange.HelpContext);
  pUpdateStatusBar;
  pFillPLUFields;
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmPLChange.ButEditActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { In module Dbase call the procedure PreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  PreEditSaveKeys;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Player Info Data Area ON }
  pPlayerInfo(True);
  { Put the cursor in the first field }
  OSFLastName.SetFocus;
End;

Procedure TfrmPLChange.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  Refresh;
  dbase.Filno := PLChange_no;
  Case iDataMode Of
    1, 3: Begin
        // function duplicate_player(const number: String; Fno,Kno: integer; Func: String)
        If Not duplicate_player(OSFPlayerNo.Text, PLChange_no, 2, 'C') Then Begin
          MailingAddressCheck;
          pCheckEditFields;
          dbase.Add_Record;
        End
        Else Begin
          If (MessageDlg('This ACBL Player Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFPlayerNo.SetFocus;
            Exit;
          End
          Else pAbortChanges; // Adding or Copying a record
        End;
      End;
    2: Begin
        If fCheckForChanges Then Begin
          { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
            in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
            with the changed data. }
          MailingAddressCheck;
          pCheckEditFields;
          frmPlayerLookUp.bKeyIsUnique := PostEditFixKeys;
          If Not frmPlayerLookUp.bKeyIsUnique Then Begin
            If (MessageDlg('This record already exists in this database.', mtError,
              [mbOK], 0) = mrOK) Then Begin
              OSFLastName.SetFocus;
              Exit;
            End
            Else pAbortChanges;
          End
          Else dbase.PutARec(PLChange_no);
        End
        Else pAbortChanges; // Editing a record
      End;
  End;
  { Turn the Player Info Data Area OFF }
  pPlayerInfo(False);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pUpdateStatusBar;
End;

Procedure TfrmPLChange.FlushAFile(Const Fno: Integer);
Var
  i, Keyno: Integer;
Begin
  FlushFile(DatF[Fno]^);
  For Keyno := 1 To MaxKeyno Do
    If (KeyLen[Fno, Keyno] > 0) Then FlushIndex(IdxKey[Fno, Keyno]^);
End;

Procedure TfrmPLChange.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  //  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  //  iDataMode := 1;
  //  sDataMode := 'A';
  //  { Turn the Player Info Data Area ON }
  //  pPlayerInfo(True);
  //  PlayernInit;
  //  pFillPLUFields;
  //  { Put the cursor in the first field }
  //  OSFLastName.SetFocus;
End;

Procedure TfrmPLChange.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  //  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  //  iDataMode := 3;
  //  sDataMode := 'C';
  //  { Turn the Player Info Data Area ON }
  //  pPlayerInfo(True);
  //  { Put the cursor in the first field }
  //  OSFLastName.SetFocus;
End;

Procedure TfrmPLChange.pSetEvents(bSwitchEventsOn: Boolean);
Begin
  If bSwitchEventsOn Then Begin
    OSFLastName.OnExit := OSFLastNameExit;
    OSFPlayerNo.OnExit := OSFPlayerNoExit;
    LMDHLCBState.OnExit := LMDHLCBStateExit;
    LMDHLCBRank.OnExit := LMDHLCBRankExit;
    LMDHLCBMail.OnExit := LMDHLCBMailExit;
    LMDHLCBGender.OnExit := LMDHLCBGenderExit;
    LMDHLCBFee.OnExit := LMDHLCBFeeExit;
    OSFDistrictNo.OnExit := OSFDistrictNoExit;
  End
  Else Begin
    OSFLastName.OnExit := Nil;
    OSFPlayerNo.OnExit := Nil;
    LMDHLCBState.OnExit := Nil;
    LMDHLCBRank.OnExit := Nil;
    LMDHLCBMail.OnExit := Nil;
    LMDHLCBGender.OnExit := Nil;
    LMDHLCBFee.OnExit := Nil;
    OSFDistrictNo.OnExit := Nil;
  End;
End;

Procedure TfrmPLChange.F3KeyExecute(Sender: TObject);
Begin
  // ---------------------------------------------------
  ActionManager1.State := asSuspended;
  frmPlayerLookUp.Hide;
  // ---------------------------------------------------
  frmTDInfo := tfrmTDInfo.Create(Nil);
  frmTDInfo.ShowModal;
  frmTDInfo.Free;
  // ---------------------------------------------------
  ActionManager1.State := asNormal;
  frmPlayerLookUp.Show;
  pFillPLUFields;
  // ---------------------------------------------------
End;

Procedure TfrmPLChange.F4KeyExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmPLChange.ShiftF4Execute(Sender: TObject);
Var
  j: word;
  iDelRecs: Integer;
Begin
  If (Filno <> PlChange_no) Or (UsedRecs(DatF[Filno]^) < 1) Then Begin
    MessageDlg('Transaction file is already empty.', mtError, [mbOK], 0);
    exit;
  End
  Else Begin
    If MessageDlg('Confirm delete transaction file', mtConfirmation, [mbYes, mbNo], 0) = mrNo Then
      exit;
    iReccount := UsedRecs(DatF[PLChange_no]^);
    For iDelRecs := iReccount Downto 0 Do Begin
      pFirstPNRecord;
      DelARec(PLChange_no);
    End;
    Close;
  End;
End;

Procedure TfrmPLChange.OpenFiles;
Var
  j, I: integer;
  HandlesLeft: byte;
  HandlesNeeded: byte;
  DBSize: Array[1..MaxFilno] Of Integer;
  DBFileOpen: Array[1..MaxFilno] Of boolean;
  DBNeedToOpen: Array[1..MaxFilno] Of boolean;
  DBFReadOnly: Array[1..MaxFilno] Of boolean;
  FOK: Boolean;
  RecAvail: Array[1..MaxFilno] Of Boolean;
  DatF: Array[1..MaxFilno] Of ^DataFile;
  IdxKey: Array[1..MaxFilno, 1..MaxKeyno] Of ^IndexFile;
  TKeyTab: Array[1..MaxKeyno] Of KeyStr;
Begin
  HandlesNeeded := 2;
  For i := 1 To MaxFilno Do For j := 0 To MaxKeyno Do
      If Length(DBNames[i, j]) > 0 Then Inc(HandlesNeeded);
  DBSize[1] := PLAYERN_Size;
  DBSize[2] := TDINFO_Size;
  DBSize[3] := PLCHANGE_Size;
  FOK := True;
  For I := 1 To MaxFilno Do If FOK And DBNeedtoOpen[I] Then
      FOK := OpenDBFile(I);
  If Not FOK Then Begin
    For I := 1 To MaxFilno Do Begin
      If DBFileOpen[I] Then Begin
        CloseDFile(DatF[I]^);
        Dispose(DatF[I]);
        For J := 1 To MaxKeyno Do If (KeyLen[I, J] > 0) Then Begin
            CloseIndex(IdxKey[I, J]^);
            Dispose(IdxKey[I, J]);
          End;
      End;
    End;
  End;
End;

Procedure TfrmPLChange.CloseFiles;
Var
  I, J: Integer;
  DBSize: Array[1..MaxFilno] Of Integer;
  DBFileOpen: Array[1..MaxFilno] Of boolean;
  DBNeedToOpen: Array[1..MaxFilno] Of boolean;
  DBFReadOnly: Array[1..MaxFilno] Of boolean;
  FOK: Boolean;
  RecAvail: Array[1..MaxFilno] Of Boolean;
  DatF: Array[1..MaxFilno] Of ^DataFile;
  IdxKey: Array[1..MaxFilno, 1..MaxKeyno] Of ^IndexFile;
  TKeyTab: Array[1..MaxKeyno] Of KeyStr;
Begin
  For I := 1 To MaxFilno Do If DBFileOpen[I] Then Begin
      CloseDFile(DatF[I]^);
      Dispose(DatF[I]);
      For J := 1 To MaxKeyno Do Begin
        If KeyLen[I, J] > 0 Then Begin
          CloseIndex(IdxKey[I, J]^);
          Dispose(IdxKey[I, J]);
        End;
      End;
    End;
  FillChar(DBFileOpen, SizeOf(DBFileOpen), false);
End;

Procedure TfrmPLChange.FormShow(Sender: TObject);
Begin
  If Not bRecCount Then keybd_event(VK_ESCAPE, MapVirtualKey(VK_ESCAPE, 0), 0, 0);
End;

Procedure TfrmPLChange.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  frmPlayerLookUp.ksPCPlayerNo := GetKey(PLchange_no, 2);
End;

Procedure TfrmPLChange.pDateFieldKeyPress(Sender: TObject; Var Key: Char);
Begin
  pEditFieldKeyPress(Sender, Key);
End;

Procedure TfrmPLChange.Action1Execute(Sender: TObject);
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinNormal := (WindowState = wsNormal);
End;

Procedure TfrmPLChange.Action2Execute(Sender: TObject);
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinNormal := (WindowState = wsNormal);
End;

Procedure TfrmPLChange.Action3Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmPLChange.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmPLChange.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
Begin
  If UpperCase(sType) = 'S' Then
    pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
  Else If UpperCase(sType) = 'P' Then
    pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
  Else If UpperCase(sType) = 'C' Then
    pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
  Else If UpperCase(sType) = 'D' Then
    pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
  Else If UpperCase(sType) = 'H' Then
    pLMDHCBFieldAvail(bDoedit, (oSender As TLMDHeaderListComboBox))
  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
  // -------------------------------------------------------------------------------------------------------
  If bDisplay_Rec Then Begin
    { Load all information to the appropriate field from the current record }
    pFillPLUFields;
  End;
  // -------------------------------------------------------------------------------------------------------
  If bWasOnEnter Then Begin
    If UpperCase(sType) = 'S' Then Begin
      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'P') Then Begin
      If Not (oSender As TOvcPictureField).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'D') Then Begin
      If Not (oSender As TOvcDateEdit).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'H') Then Begin
      If Not (oSender As TLMDHeaderListComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'C') Then Begin
      If Not (oSender As TOvcComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End;
  End;
End;

Procedure TfrmPLChange.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPLChange.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPLChange.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPLChange.pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPLChange.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPLChange.pResetFldAttrib;
Var
  iComponent: integer;
Begin
  With Self Do Begin
    For iComponent := 0 To ComponentCount - 1 Do Begin
      If Components[iComponent].ClassType = TOvcSimpleField Then
        pOSFieldAvail(True, (Components[iComponent] As TOvcSimpleField))
      Else If Components[iComponent].ClassType = TOvcPictureField Then
        pOPFieldAvail(True, (Components[iComponent] As TOvcPictureField))
      Else If Components[iComponent].ClassType = TOvcDateEdit Then
        pODEFieldAvail(True, (Components[iComponent] As TOvcDateEdit))
      Else If Components[iComponent].ClassType = TOvcComboBox Then
        pOCBFieldAvail(True, (Components[iComponent] As TOvcComboBox));
    End;
  End;
End;

Procedure TfrmPLChange.OSFLastNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.LAST_NAME := Pad(OSFLastName.Text,16);
  bDoEdit := CustomPLayerEdit(Scrno, 3, PLchange.LAST_NAME, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFLastNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      { Validation of Last Name field when exiting it }
      If trim(OSFLastName.Text) = '' Then Begin
        If MessageDlg('Last Name must NOT be blank! ' + #13 + #10 + '' + #13 + #10 +
          '        Correct this error?', mtError, [mbYes, mbNo], 0) = MRNO Then Begin
          pAbortChanges;
        End
        Else OSFLastName.SetFocus
      End
      Else Begin
        PLchange.LAST_NAME := Pad(OSFLastName.Text,16);
        bDoEdit := CustomPLayerEdit(Scrno, 3, PLchange.LAST_NAME, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPLChange.OSFFirstNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.FIRST_NAME := Pad(OSFFirstName.Text,16);
  bDoEdit := CustomPLayerEdit(Scrno, 2, PLchange.FIRST_NAME, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFFirstNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.FIRST_NAME := Pad(OSFFirstName.Text,16);
      bDoEdit := CustomPLayerEdit(Scrno, 2, PLchange.FIRST_NAME, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPLChange.OSFPlayerNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  bDoEdit := CustomPLayerEdit(Scrno, 1, PLchange.PLAYER_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFPlayerNoExit(Sender: TObject);
Var
  osPlayer_no: OpenString;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      osPlayer_no := OSFPlayerNo.Text;
      { Check for a duplicate Player Number }
      If Not (player_check(osPlayer_no, pnAny)) Then Begin
        ErrBox('Invalid ACBL Player Number!', mc, 0);
        OSFPlayerNo.SetFocus;
      End
      Else Begin
        PLchange.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
        bDoEdit := CustomPLayerEdit(Scrno, 1, PLchange.PLAYER_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPLChange.OSFStreet1Enter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.Street1 := Pad(OSFStreet1.Text,30);
  bDoEdit := CustomPLayerEdit(Scrno, 4, PLchange.Street1, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFStreet1Exit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.Street1 := Pad(OSFStreet1.Text,30);
      bDoEdit := CustomPLayerEdit(Scrno, 4, PLchange.Street1, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPLChange.OSFStreet2Enter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.Street2 := Pad(OSFStreet2.Text,26);
  bDoEdit := CustomPLayerEdit(Scrno, 5, PLchange.Street2, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFStreet2Exit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.Street2 := Pad(OSFStreet2.Text,26);
      bDoEdit := CustomPLayerEdit(Scrno, 5, PLchange.Street2, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPLChange.OSFCityEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.CITY := Pad(OSFCity.Text,16);
  bDoEdit := CustomPLayerEdit(Scrno, 6, PLchange.CITY, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFCityExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.CITY := Pad(OSFCity.Text,16);
      bDoEdit := CustomPLayerEdit(Scrno, 6, PLchange.CITY, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPLChange.LMDHLCBStateEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  PLchange.STATE := Pad(LMDHLCBState.Text,2);
  bDoEdit := CustomPLayerEdit(Scrno, 7, PLchange.STATE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPLChange.LMDHLCBStateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidStateCode(Playern.Country, LMDHLCBState.Text)) Then Begin
        ErrBox('Invalid State/Province code.', mc, 0);
        LMDHLCBState.Text := '';
        LMDHLCBCountry.Text := '';
        LMDHLCBState.SetFocus;
      End
      Else Begin
        Playern.STATE := LMDHLCBState.Text;
        bDoEdit := CustomPLayerEdit(Scrno, 7, Playern.STATE, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'H');
        LMDHLCBCountry.Text := Playern.Country;
      End;
    End;
  End;
End;

Procedure TfrmPLChange.OPFZipEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.ZIP := Pad(OPFZip.Text,2);
  bDoEdit := CustomPLayerEdit(Scrno, 8, PLchange.ZIP, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPLChange.OPFZipExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.ZIP := Pad(OPFZip.Text,10);
      bDoEdit := CustomPLayerEdit(Scrno, 8, PLchange.ZIP, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmPLChange.LMDHLCBCountryEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  PLchange.Country := Pad(LMDHLCBCountry.Text,2);
  bDoEdit := CustomPLayerEdit(Scrno, 9, PLchange.Country, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPLChange.LMDHLCBCountryExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidCountryCode(LMDHLCBCountry.Text, true)) Then Begin
        ErrBox('Invalid Country Code', mc, 0);
        LMDHLCBCountry.Text := '';
        LMDHLCBCountry.SetFocus;
      End
      Else Begin
        PLchange.Country := Pad(LMDHLCBCountry.Text,2);
        bDoEdit := CustomPLayerEdit(Scrno, 9, PLchange.Country, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'H');
      End;
    End;
  End;
End;

Procedure TfrmPLChange.OSFEmailEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.email := Pad(OSFEmail.Text,40);
  bDoEdit := CustomPLayerEdit(Scrno, 10, PLchange.email, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFEmailExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (Empty(OSFEmail.Text) Or (ValidEmailAddress(OSFEmail.Text, 40) > 0)) Then Begin
        ErrBox('Email address is not valid', mc, 0);
        OSFEmail.SetFocus;
      End
      Else Begin
        PLchange.email := Pad(OSFEmail.Text,40);
        bDoEdit := CustomPLayerEdit(Scrno, 10, PLchange.email, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPLChange.LMDHLCBRankEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  PLchange.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 12, PLchange.ACBL_RANK, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPLChange.LMDHLCBRankExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Ranking Codes:

         Rookie (blank)     0 - 5
      A  Junior Master      5 - 20
      B  Club Master       20 - 50
      C  Sectional Master  50 - 100
      D  Regional Master  100 - 200
      E  NAC Master       200 - 300
      F  Life Master      300 - 500
      G  Bronze LM        500 - 1000
      H  Silver LM       1000 - 2500
      I  Gold LM         2500 - 5000
      J  Diamond LM      5000 - 7500
      K  Emerald LM      7500 - 10000
      L  Platinum LM     over   10000
      M  Grand LM NABC   over   10000
      X  Rank not known
      S  Suspended
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
      bDoEdit := CustomPLayerEdit(Scrno, 12, PLchange.ACBL_RANK, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBRank.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sRankCode) Do Begin
          If LMDHLCBRank.Text = libdata.sRankCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then LMDHLCBRank.Text := '';
    End;
  End;
End;

Procedure TfrmPLChange.OPFCatBEnter(Sender: TObject);
Begin
  { Used 13 for the field code since it was not assigned in I_Update.Inc }
  bDoEdit := True;
  PLchange.CAT_B := Pad(OPFCatB.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 21, PLchange.CAT_B, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPLChange.OPFCatBExit(Sender: TObject);
Begin
  { Used 13 for the field code since it was not assigned in I_Update.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((OPFCatB.Text = ' ') Or (OPFCatB.Text = 'P') Or (OPFCatB.Text = 'E')) Then Begin
        ErrBox('Paid pending / Exempt must be P or E or blank', mc, 0);
        OPFCatB.Text := '';
        OPFCatB.SetFocus;
      End
      Else Begin
        PLchange.CAT_B := Pad(OPFCatB.Text,1);
        bDoEdit := CustomPLayerEdit(Scrno, 21, PLchange.CAT_B, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

Procedure TfrmPLChange.LMDHLCBMailEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  PLchange.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 11, PLchange.MAIL_CODE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPLChange.LMDHLCBMailExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Mail Codes:

        (blank) Mailing label will NOT be printed
      M Mailing label will be printed
      U Address unknown
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
      bDoEdit := CustomPLayerEdit(Scrno, 11, PLchange.MAIL_CODE, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBMail.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sMailCode) Do Begin
          If LMDHLCBMail.Text = libdata.sMailCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then LMDHLCBMail.Text := '';
    End;
  End;
End;

Procedure TfrmPLChange.OSFUnitNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
  bDoEdit := CustomPLayerEdit(Scrno, 19, PLchange.UNIT_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFUnitNoExit(Sender: TObject);
Var
  osUnitNo: OpenString;
  //  bValidUnitNo: Boolean;
Begin
  //  bValidUnitNo := LibData.fValidUnit(osUnitNo);
  //  If bValidUnitNo Then OSFDistrictNo.Text := LibData.sDistrictNumber;
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      osUnitNo := OSFUnitNo.Text;
      If Not (fValidUnit(osUnitNo)) Then Begin
        ErrBox('Invalid Unit number', mc, 0);
        OSFUnitNo.Text := '';
        OSFUnitNo.SetFocus;
      End
      Else Begin
        Playern.UNIT_NO := OSFUnitNo.Text;
        bDoEdit := CustomPLayerEdit(Scrno, 19, Playern.UNIT_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPLChange.OSFDistrictNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.District_no := FixNumField(OSFDistrictNo.Text,2);
  bDoEdit := CustomPLayerEdit(Scrno, 20, PLchange.District_no, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPLChange.OSFDistrictNoExit(Sender: TObject);
Var
  osDistrictNo: OpenString;
  //  bValidDistrictNo: Boolean;
Begin
  osDistrictNo := OSFDistrictNo.Text;
  //  bValidDistrictNo := LibData.fValidDistrict(osDistrictNo);
  //  If Not bValidDistrictNo Then OSFDistrictNo.SetFocus;
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (fValidDistrict(osDistrictNo)) Then Begin
        ErrBox('Invalid District number', mc, 0);
        OSFDistrictNo.Text := '';
        OSFDistrictNo.SetFocus;
      End
      Else Begin
        PLchange.District_no := FixNumField(OSFDistrictNo.Text,2);
        bDoEdit := CustomPLayerEdit(Scrno, 20, PLchange.District_no, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPLChange.LMDHLCBGenderEnter(Sender: TObject);
Begin
  { Used 18 for the field code since it was not assigned in I_Update.Inc }
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  PLchange.GENDER := Pad(LMDHLCBGender.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 13, PLchange.GENDER, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPLChange.LMDHLCBGenderExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Gender Codes:

        (blank) Unknown
      F Female
      M Male
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.GENDER := Pad(LMDHLCBGender.Text,1);
      bDoEdit := CustomPLayerEdit(Scrno, 13, PLchange.GENDER, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBGender.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sGenderCode) Do Begin
          If LMDHLCBGender.Text = libdata.sGenderCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then Begin
        ErrBox('Gender must be ( )Blank, (M)ale or (F)emale', mc, 0);
        LMDHLCBGender.SetFocus;
      End;
    End;
  End;
End;

Procedure TfrmPLChange.LMDHLCBFeeEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  PLchange.Fee_type := Pad(LMDHLCBFee.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 22, PLchange.Fee_type, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPLChange.LMDHLCBFeeExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Fee Codes:

        (blank) None of the above
      H Household
      J Student
      L Life master reduced
      M LM paying regular fees
      N Non life master regular
      P Patron
      X Patron Household
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.Fee_type := Pad(LMDHLCBFee.Text,1);
      bDoEdit := CustomPLayerEdit(Scrno, 22, PLchange.Fee_type, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If (LMDHLCBFee.Focused) Then Begin
        If LMDHLCBFee.Text <> ' ' Then Begin
          bValid := False;
          For iForLoop := 1 To length(libdata.sFeeCode) Do Begin
            If LMDHLCBFee.Text = libdata.sFeeCode[iForLoop] Then Begin
              bValid := True;
              break
            End;
          End;
        End
        Else bValid := True;
        If Not bValid Then Begin
          ErrBox('Invalid Fee Code', mc, 0);
          LMDHLCBFee.SetFocus;
        End;
      End;
    End;
  End;
End;

Procedure TfrmPLChange.OPFPaidThruEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.Paid_Thru := Pad(OPFPaidThru.Text,6);
  bDoEdit := CustomPLayerEdit(Scrno, 18, PLchange.Paid_Thru, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPLChange.OPFPaidThruExit(Sender: TObject);
Var
  sPaidThru: String;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      sPaidThru := Copy(OPFPaidThru.Text, 1, 2) + Copy(OPFPaidThru.Text, 4, 4);
      If Not (Empty(sPaidThru) Or chkdate(Copy(sPaidThru, 1, 2) + '01' + Copy(sPaidThru, 3, 4))) Then Begin
        ErrBox('Invalid month/year', mc, 0);
        OPFPaidThru.SetFocus;
      End
      Else Begin
        PLchange.Paid_Thru := Pad(OPFPaidThru.Text,6);
        bDoEdit := CustomPLayerEdit(Scrno, 18, PLchange.Paid_Thru, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

Procedure TfrmPLChange.ButtonF1KeyClick(Sender: TObject);
Begin
  If HelpContext > 0 Then keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmPLChange.OPFPhoneEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLchange.PHONE := Pad(OPFPhone.Text,20);
  bDoEdit := CustomPLayerEdit(Scrno, 14, PLchange.PHONE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPLChange.OPFPhoneExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLchange.PHONE := Pad(OPFPhone.Text,20);
      bDoEdit := CustomPLayerEdit(Scrno, 14, PLchange.PHONE, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

function TfrmPLChange.FormHelp(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  if data < 500000 then WinHelp(Application.Handle,'ACBLSCORE.HLP',Help_Context,Data);
end;

End.

