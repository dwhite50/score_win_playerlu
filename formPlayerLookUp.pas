Unit formPlayerLookUp;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, dbSizes, PVIO, VListBox, Clubfees, history, mtables,
  groups,hh,hh_funcs;

Type
  { Required for Util2.Player_Check Function }
  OpenString = String[10];
  _DateString = String[8];

Type
  TfrmPlayerLookUp = Class(TForm)
    ResizeKit1: TResizeKit;
    OvcController1: TOvcController;
    StaticText1: TStaticText;
    GBPlayerInfo: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Shape1: TShape;
    Label3: TLabel;
    Shape2: TShape;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Shape3: TShape;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Shape4: TShape;
    Shape5: TShape;
    Label15: TLabel;
    Shape6: TShape;
    Label19: TLabel;
    Label20: TLabel;
    Shape7: TShape;
    Shape8: TShape;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Shape13: TShape;
    Label25: TLabel;
    Shape14: TShape;
    Label26: TLabel;
    ButDonePlayerInfo: TButton;
    LMDHLCBState: TLMDHeaderListComboBox;
    LMDHLCBCountry: TLMDHeaderListComboBox;
    LMDHLCBRank: TLMDHeaderListComboBox;
    LMDHLCBMail: TLMDHeaderListComboBox;
    LMDHLCBGender: TLMDHeaderListComboBox;
    LMDHLCBFee: TLMDHeaderListComboBox;
    OSFLastName: TOvcSimpleField;
    OSFFirstName: TOvcSimpleField;
    OSFPlayerNo: TOvcSimpleField;
    OSFStreet1: TOvcSimpleField;
    OSFStreet2: TOvcSimpleField;
    OSFCity: TOvcSimpleField;
    OSFEmail: TOvcSimpleField;
    OPFPaidThru: TOvcPictureField;
    OPFZip: TOvcPictureField;
    OSFUnitNo: TOvcSimpleField;
    OSFDistrictNo: TOvcSimpleField;
    LMDButPlayerInfoDone: TLMDButton;
    OPFCatB: TOvcPictureField;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    GBMasterPoints: TGroupBox;
    Label28: TLabel;
    Shape17: TShape;
    Label29: TLabel;
    Shape18: TShape;
    Label30: TLabel;
    Shape19: TShape;
    Label31: TLabel;
    Shape20: TShape;
    Label32: TLabel;
    OPFPTotal: TOvcPictureField;
    OPFPYTD: TOvcPictureField;
    OPFPMTD: TOvcPictureField;
    OPFPRecent: TOvcPictureField;
    OPFPEligibility: TOvcPictureField;
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF4Key: TButton;
    ButtonF3Key: TButton;
    ButtonF5Key: TButton;
    ButtonF2Key: TButton;
    ButtonF7Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    StaticTextF7Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ImageList1: TImageList;
    ActionManager3: TActionManager;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    Button1: TButton;
    StaticText3: TStaticText;
    CtrlCKey: TAction;
    StatusBar1: TStatusBar;
    StaticText2: TStaticText;
    LMDHint1: TLMDHint;
    Label16: TLabel;
    Shape15: TShape;
    Label17: TLabel;
    OSFLastImpDate: TOvcSimpleField;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    OPFPhone: TOvcPictureField;
    OPFLocalActiveDate: TOvcPictureField;
    OPFAcblActiveDate: TOvcPictureField;
    OPFLastACBLUpdate: TOvcPictureField;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Procedure FormActivate(Sender: TObject);
    Procedure CtrlCKeyExecute(Sender: TObject);
    Procedure pFillPLUFields;
    Procedure pNextPNRecord;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure pPrevPNRecord;
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure pFirstPNRecord;
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pGBMasterPoints(bEnable: Boolean);
    Procedure PrintPlayer;
    Procedure F7KeyExecute(Sender: TObject);
    Procedure MasterPointsKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pMasterPointsDone;
    Procedure pSaveMPChanges;
    Procedure pAbortMPChanges;
    Procedure OPFPEligibilityExit(Sender: TObject);
    Procedure pGBPlayerInfoReadOnly(bLogical: Boolean);
    Procedure pGBMasterPointsReadOnly(bLogical: Boolean);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pAddToPlChange(Const AskUser: boolean);
    Procedure OSFLastNameExit(Sender: TObject);
    Procedure OSFPlayerNoExit(Sender: TObject);
    Procedure ComboBoxChange(Sender: TObject);
    Procedure ComboBoxEnter(Sender: TObject);
    Function fFindCBItemIndex(cbName: TObject; sText2Find: String): Integer;
    Procedure LMDHLCBEnter(Sender: TObject);
    Procedure OSFDistrictNoExit(Sender: TObject);
    Procedure pDateExit(Sender: TObject);
    Procedure LMDHLCBRankExit(Sender: TObject);
    Procedure LMDHLCBMailExit(Sender: TObject);
    Procedure LMDHLCBGenderExit(Sender: TObject);
    Procedure LMDHLCBFeeExit(Sender: TObject);
    Procedure FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure FormKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure MailingAddressCheck;
    Procedure OSFUnitNoExit(Sender: TObject);
    Procedure pAbortChanges;
    Function Status_OK(Const Fno: Integer): Boolean;
    Function fGetLength(iFileNo: Integer; sFieldName: String): Integer;
    Procedure pCheckEditFields;
    Function fCheckForChanges: Boolean;
    Procedure pCopyRecData;
    Procedure pSaveChanges;
    Procedure pPlayerInfo(bEnable: Boolean);
    Procedure pUpdateStatusBar;
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure FlushAFile(Const Fno: Integer);
    Procedure F5KeyExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure pSetEvents(bSwitchEventsOn: Boolean);
    Procedure OPFPaidThruExit(Sender: TObject);
    Procedure F3KeyExecute(Sender: TObject);
    Procedure AlignFile(Const Fno, Kno: byte; Const KeyToFind: String; Const ShowRec: boolean);
    Procedure F4KeyExecute(Sender: TObject);
    Procedure pDateFieldKeyPress(Sender: TObject; Var Key: Char);
    Procedure OPFPTotalExit(Sender: TObject);
    Procedure OPFPYTDExit(Sender: TObject);
    Function fShowPoints(Const Ptype: byte): String;
    Procedure SetPoints(Const j: byte);
    Procedure FixPoints(Const j: byte);
    Procedure pStoreMPoints;
    Procedure pRefreshMPFields;
    Procedure OPFPMTDExit(Sender: TObject);
    Procedure OPFPRecentExit(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure Action3Execute(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    Procedure Action5Execute(Sender: TObject);
    Procedure Action6Execute(Sender: TObject);
    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    Procedure pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
    Procedure pResetFldAttrib;
    Procedure OSFLastNameEnter(Sender: TObject);
    Procedure OSFFirstNameEnter(Sender: TObject);
    Procedure OSFFirstNameExit(Sender: TObject);
    Procedure OSFPlayerNoEnter(Sender: TObject);
    Procedure OSFStreet1Enter(Sender: TObject);
    Procedure OSFStreet1Exit(Sender: TObject);
    Procedure OSFStreet2Enter(Sender: TObject);
    Procedure OSFStreet2Exit(Sender: TObject);
    Procedure OSFCityEnter(Sender: TObject);
    Procedure OSFCityExit(Sender: TObject);
    Procedure LMDHLCBStateEnter(Sender: TObject);
    Procedure LMDHLCBStateExit(Sender: TObject);
    Procedure OPFZipEnter(Sender: TObject);
    Procedure OPFZipExit(Sender: TObject);
    Procedure LMDHLCBCountryEnter(Sender: TObject);
    Procedure LMDHLCBCountryExit(Sender: TObject);
    Procedure OSFEmailEnter(Sender: TObject);
    Procedure OSFEmailExit(Sender: TObject);
    Procedure LMDHLCBRankEnter(Sender: TObject);
    Procedure LMDHLCBMailEnter(Sender: TObject);
    Procedure OSFUnitNoEnter(Sender: TObject);
    Procedure OSFDistrictNoEnter(Sender: TObject);
    Procedure LMDHLCBGenderEnter(Sender: TObject);
    Procedure LMDHLCBFeeEnter(Sender: TObject);
    Procedure OPFCatBEnter(Sender: TObject);
    Procedure OPFCatBExit(Sender: TObject);
    Procedure OPFPaidThruEnter(Sender: TObject);
    Procedure ButtonF1KeyClick(Sender: TObject);
    Procedure OPFPhoneEnter(Sender: TObject);
    Procedure OPFPhoneExit(Sender: TObject);
    function FormHelp(Command: Word; Data: Integer;
      var CallHelp: Boolean): Boolean;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    { Integer variable used dynamically to store the tables record count }
    iReccount: Integer;
    sStandFuncKeyHint: String;
    { iDataMode Integer Variable
      Set the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
    //iDataMode: Integer;
    ksPCPlayerNo: KeyStr;
    bKeyIsUnique: Boolean;
    Pnt: TPoint;
  End;

Var
  mHHelp: THookHelpSystem;      
  frmPlayerLookUp: TfrmPlayerLookUp;
  ksPlayerN, ksPlayerNTemp: keystr;
  ksPLChange, ksPLChangeTemp: keystr;
  //ksTDInfo, ksTDInfoTemp: keystr;
  ksFirstKey: KeyStr;
  aOK: Boolean;
  iPosChar: Integer;
  i001, i002, i003, iValue, iCode: Integer;
  rValue: Real;
  s001: String;
  bOffice: Boolean;
  bCompare: Boolean;
  sOldElgPts, sCompStr: String;
  aPpoints: Array[1..5] Of real;
  rOldepts: real;
  aOldpoints: Array[1..5] Of String[4];
  sOldGroup: String;
  bGroupDirty: boolean;

Const
  bStart: Boolean = False;
  bWinNormal: Boolean = True;
  bEscKeyUsed: Boolean = False;
  Scrno: Integer = 1;
  { iDataMode Integer Variable
    Set the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode: Integer = 0;
  { sDataMode is the Char representation of iDataMode }
  sDataMode: Char = Null;
  bDoEdit: Boolean = True;
  bDisplay_Rec: Boolean = False;
  bF1KeyUsed: Boolean = False;

  {$I fdbnames.dcl}

Function duplicate_player(Const number: String; Fno, Kno: integer; Func: String): boolean;

Implementation

Uses formTDInfo, formPLChange, ConvUtils;

{$DEFINE indatabase}
{$DEFINE isnoattend}
{$I STSTRS.dcl}
{$I showpts.inc}
{$I addform.inc}
{$I fixdb.inc}
{$I i_player.inc}

Procedure PleaseWait;
Begin
  PlsWait;
End;

{$R *.dfm}

Procedure TfrmPlayerLookUp.ButQuitActionExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmPlayerLookUp.FormClose(Sender: TObject;
  Var Action: TCloseAction);
Begin
  { Close any open tables }
  fdBase.fCloseFiles;
  dBase.CloseFiles;
End;

Procedure TfrmPlayerLookUp.FormActivate(Sender: TObject);
Begin
  If bStart Then exit;
  bStart := True;
  { Must be set to true for Win32 applications. Used in Dbase and Fdbase }
  UseDBLinkages := True;
  Scrno := 1;
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Tourn_no; // ClubDef_no;
  ksPCPlayerNo := '';
  UseLookupDB := True;
  Application.HintHidePause := 200000;
  sStandFuncKeyHint := 'Use this Function Key to ';
  { Procedure to check and see if the Tables were closed correctly }
  TestBadDbase;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is made to a "Record",
    dbase re-writes the table header and in Playern.dat, updates dbLastImpDate entry. Set them to
    False when you Delete a record, then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  //==========================================================
  { These variables are declared in CSDef.pas }
  IniName := GetCurrentDir + '\' + DefIniName;
  AppName := 'PlayerLookUp';
  //==========================================================
  CSDef.ReadCFG;
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ============================================================================
  { If we are using the player file at the office, it will be on drive E:
    PLPath Contains the Player Lookup Path information }
  Prepend := FixPath(CFG.PLPath);
  iPosChar := pos('E:\', UpperCase(Prepend));
  If iPosChar > 0 Then Begin
    { We ARE using ACBLscore at the ACBL Office Site }
    {$DEFINE Office};
    bOffice := True;
    ButEdit.Enabled := False;
    ButEditAction.Enabled := False;
    ButAdd.Enabled := False;
    ButAddAction.Enabled := False;
    ButCopy.Enabled := False;
    ButCopyAction.Enabled := False;
    ButDelete.Enabled := False;
    ButDeleteAction.Enabled := False;
  End
  Else bOffice := False; { We ARE NOT using ACBLscore at the ACBL Office Site }
  // ----------------------------------------------------------------------------------------------------------
  { Begin opening the tables. If anyone fails to open, close the form }
  If Not OpenDBFile(Playern_no) Then Close;
  If Not fOpenDBFile(TDInfo_no) Then Close;
  If Not OpenDBFile(PlChange_no) Then Close;
  // -----------------------------------------
  { Store the number of records in PLAYERN.DAT to iReccount }
  iReccount := db33.UsedRecs(DatF[Playern_no]^);
  dBase.Filno := Playern_no;
  fDbase.fFilno := Tdinfo_no;
  // -----------------------------------------
  ClearKey(IdxKey[Playern_no, 1]^);
  If iReccount > 0 Then Begin
    { Find the first available record and load it's values to the fields on the screen }
    pFirstPNRecord;
  End
  Else Begin
    // If there are no Player Records
    Close;
  End;
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'PLAYERN.DAT');
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iReccount)) + ' Records.';
  { Set Events for Edit Fields }
  pSetEvents(False);
End;

Procedure TfrmPlayerLookUp.CtrlCKeyExecute(Sender: TObject);
Var
  wcbClipText: tClipboard;
Begin
  wcbClipText := tClipboard.Create;
  wcbClipText.Clear;
  If Not Empty(OSFEmail.Text) Then Begin
    wcbClipText.AsText := ('"' + Trim(OSFFirstName.Text) + ' ' + Trim(OSFLastName.Text) + '" <'
      + Trim(OSFEmail.Text) + '>');
    ErrBox('Email address has been copied to Windows Clipboard', MC, 0);
  End
  Else ErrBox('Email address is empty', MC, 0);
  wcbClipText.Free;
End;

Procedure TfrmPlayerLookUp.pFillPLUFields;
Var
  sEmail: String;
  rPoints: Extended;
Begin
  // -----------------------------------------------------------
  { The field CatA is not used in the Player Lookup Module! }
  // -----------------------------------------------------------
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  OSFLastName.Text := Trim(Playern.LAST_NAME);
  OSFFirstName.Text := Trim(Playern.FIRST_NAME);
  OSFPlayerNo.Text := Trim(Playern.PLAYER_NO);
  OSFStreet1.Text := Trim(Playern.STREET1);
  OSFStreet2.Text := Trim(Playern.STREET2);
  OSFCity.Text := Trim(Playern.CITY);
  // STATE   ----------------------------------------------------------------------
  LMDHLCBState.Text := Trim(Playern.STATE);
  // ------------------------------------------------------------------------------
  OPFZip.Text := Trim(Playern.ZIP);
  // COUNTRY ----------------------------------------------------------------------
  LMDHLCBCountry.Text := Trim(Playern.COUNTRY);
  // ------------------------------------------------------------------------------
  sEmail := Trim(Playern.EMAIL);
  OSFEmail.Text := Trim(Playern.EMAIL);
  // ------------------------------------------------------------------------------
  OPFPhone.Text := Trim(Playern.PHONE);
  // ------------------------------------------------------------------------------
  OPFLocalActiveDate.Text := Playern.LOCAL_DATE;
  OPFAcblActiveDate.Text := Playern.ACBL_DATE;
  // ------------------------------------------------------------------------------
  LMDHLCBRank.Text := Playern.ACBL_RANK;
  // ------------------------------------------------------------------------------
  OPFCatB.Text := Playern.CAT_B;
  // ------------------------------------------------------------------------------
  LMDHLCBMail.Text := Playern.MAIL_CODE;
  // ------------------------------------------------------------------------------
  OSFUnitNo.Text := Trim(Playern.UNIT_NO);
  OSFDistrictNo.Text := Trim(Playern.DISTRICT_NO);
  // ------------------------------------------------------------------------------
  LMDHLCBGender.Text := Playern.GENDER;
  LMDHLCBFee.Text := Playern.FEE_TYPE;
  // ------------------------------------------------------------------------------
  OPFPaidThru.Text := Playern.PAID_THRU;
  // ------------------------------------------------------------------------------
  OPFLastACBLUpdate.Text := Playern.ACBL_UPDATE_DATE;
  // ==============================================================================================
  pStoreMPoints;
  sOldElgPts := FloatToStr(Key2Num(PLAYERN.Elig_pts) * 0.010);
  //pFixPoints;
  { Calculate the Players Total Points }
  sCompStr := fShowPoints(4);
  OPFPTotal.Text := sCompStr;
  //OPFPTotal.Text := FloatToStr(Key2Num(PLAYERN.TOT_POINTS) * 0.010);
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points YTD }
  sCompStr := fShowPoints(3);
  OPFPYTD.Text := sCompStr;
  //OPFPYTD.Text := FloatToStr(Key2Num(PLAYERN.Y_T_D) * 0.010);
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points MTD }
  sCompStr := fShowPoints(2);
  OPFPMTD.Text := sCompStr;
  //OPFPMTD.Text := FloatToStr(Key2Num(PLAYERN.M_T_D) * 0.010);
  // ------------------------------------------------------------------------------------
  { Calculate the Players Recent Points }
  sCompStr := fShowPoints(1);
  OPFPRecent.Text := sCompStr;
  //OPFPRecent.Text := FloatToStr(Key2Num(PLAYERN.TOURN_PTS) * 0.010);
  // ------------------------------------------------------------------------------------
  { Calculate the Players Eligibility Points }
  sCompStr := fShowPoints(0);
  OPFPEligibility.Text := sCompStr;
  //OPFPEligibility.Text := FloatToStr(Key2Num(PLAYERN.Elig_pts) * 0.010);
  If LastImpDate > 0 Then
    OSFLastImpDate.Text := DateToStr(FileDateToDateTime(LastImpDate))
  Else
    OSFLastImpDate.Text := '00/00/1980';
End;

Procedure TfrmPlayerLookUp.ButNextActionExecute(Sender: TObject);
Begin
  pNextPNRecord; // Move the record pointer in the table to the next available record and load the data in the screen fields
  If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
End;

Procedure TfrmPlayerLookUp.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevPNRecord; // Move the record pointer in the table to the previous record and load the data in the screen fields
  If ButPrev.Enabled Then ButPrev.SetFocus Else ButQuit.SetFocus;
End;

Procedure TfrmPlayerLookUp.pPrevPNRecord;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen
    fields }
  Prev_Record;
  pFillPLUFields;
End;

Procedure TfrmPlayerLookUp.pNextPNRecord;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  Next_Record;
  pFillPLUFields;
End;

Procedure TfrmPlayerLookUp.ButTopActionExecute(Sender: TObject);
Begin
  pFirstPNRecord;
End;

Procedure TfrmPlayerLookUp.pFirstPNRecord;
Begin
  { Find the first available record and load it's values to the fields on the screen }
  Top_Record;
  pFillPLUFields;
End;

Procedure TfrmPlayerLookUp.ButLastActionExecute(Sender: TObject);
Begin
  { Find the last available record and load it's values to the fields on the screen }
  Last_Record;
  pFillPLUFields;
End;

Procedure TfrmPlayerLookUp.ButFindActionExecute(Sender: TObject);
Var
  bFoundMatch: Boolean;
Begin
  // --------------------------------------------------
  frmPlayerLookUp.Enabled := False;
  ActionManager1.State := asSuspended;
  // --------------------------------------------------
  { Display the find dialog box }
  bFoundMatch := Find_Record('', '', '', MC, 0, True);
  // --------------------------------------------------
  frmPlayerLookUp.Enabled := True;
  ActionManager1.State := asNormal;
  // --------------------------------------------------
  If bFoundMatch Then Begin
    { Display the selected record }
    GetARec(Playern_no);
    { Must run Align_Rec to align children records for this record }
    Align_Rec(RecNo[Filno]);
    // ------------------------------------------------------
    { ksPlayerN, ksPlayerNTemp: keystr;
      ksPLChange, ksPLChangeTemp: keystr;
      ksTDInfo, ksTDInfoTemp: keystr;}
    ksPlayerN := GetKey(PlayerN_no, 1);
    ksPlayerNTemp := ksPlayerN;
    pFillPLUFields;
  End
  Else MessageDlg('Name not found!', mtInformation, [mbOK], 0); ;
End;

Procedure TfrmPlayerLookUp.F2KeyExecute(Sender: TObject);
Begin
  { Enable the Group Box for Master Points for Editing }
  pGBMasterPoints(True);
End;

Procedure TfrmPlayerLookUp.pGBMasterPoints(bEnable: Boolean);
Begin
  { Turn the Player Info Data Area ON }
  { Allow or Deny access to the Player Information fields }
  { Set the state of ActionManager1 for the form players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  GBMasterPoints.Enabled := bEnable;
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 2;
  sDataMode := 'E';
  sOldElgPts := FloatToStr(Key2Num(PLAYERN.Elig_pts) * 0.010);
  OPFPTotal.SetFocus;
End;

Procedure TfrmPlayerLookUp.PrintPlayer;
Var
  today: String[6];
  isrenewal: boolean;
  esc: boolean;
Begin
  esc := false;
  Today := Copy(sysdate, 5, 4) + Copy(sysdate, 1, 2);
  With Playern Do Begin
    isrenewal := Player_Check(Player_no, pnACBL)
      And ((Copy(paid_thru, 3, 4) + Copy(paid_thru, 1, 2) <= Today) Or YesNoBox(esc, false,
      'Include membership renewal form', 0, MC, Nil));
  End;
  If esc Then exit;
  PrintAddressForm(true, isrenewal);
End;

Procedure TfrmPlayerLookUp.F7KeyExecute(Sender: TObject);
Begin
  PrintPlayer;
End;

Procedure TfrmPlayerLookUp.MasterPointsKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = VK_RETURN Then Begin
    key := 0;
    keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortMPChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    If iDataMode > 0 Then pSaveMPChanges;
  End;
End;

Procedure TfrmPlayerLookUp.pAbortMPChanges;
Begin
  { Read data base file Playern_no.  Record number in Recno[Playern_no]
    Reload the record before any changes where made. }
  GetARec(Playern_no);
  { Calculate the Players Total Points }
  sCompStr := fShowPoints(4);
  OPFPTotal.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points YTD }
  sCompStr := fShowPoints(3);
  OPFPYTD.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points MTD }
  sCompStr := fShowPoints(2);
  OPFPMTD.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Recent Points }
  sCompStr := fShowPoints(1);
  OPFPRecent.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Eligibility Points }
  sCompStr := fShowPoints(0);
  pStoreMPoints;
  OPFPEligibility.Text := sCompStr;
  pMasterPointsDone;
End;

Procedure TfrmPlayerLookUp.pSaveMPChanges;
Var
  J, k: byte;
Begin
  aPpoints[1] := OPFPEligibility.AsExtended;
  aPpoints[2] := OPFPRecent.AsExtended;
  aPpoints[3] := OPFPMTD.AsExtended;
  aPpoints[4] := OPFPYTD.AsExtended;
  aPpoints[5] := OPFPTotal.AsExtended;
  For J := 1 To 5 Do Begin
    // FixPoints(j);
    For k := 1 To 5 Do SetPoints(k);
  End;
  { Save the Changes back to the Database }
  dbase.PutARec(Playern_no);
  { Set the form back to normal view mode }
  pMasterPointsDone;
  pFillPLUFields;
End;

Procedure TfrmPlayerLookUp.pMasterPointsDone;
Begin
  pResetFldAttrib;
  { Make the GroupBox GBMasterPoints read only, all Maskedits are in this GB, so they by
    default, are read only also }
  pGBMasterPointsReadOnly(True);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  bEscKeyUsed := False;
  ActionManager1.State := asNormal;
  // ----------------------------------------------------------------------
  GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := True;
  GBMenu.Align := alBottom;
  // ----------------------------------------------------------------------
  ButNext.SetFocus;
End;

Procedure TfrmPlayerLookUp.OPFPEligibilityExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[1] := OPFPEligibility.AsExtended;
  J := 1;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
  { Save changes to the Master Points }
  pSaveMPChanges;
End;

Procedure TfrmPlayerLookUp.pGBPlayerInfoReadOnly(bLogical: Boolean);
Begin
  { pGBPlayerInfoReadOnly: Sets the enabled property of GBPlayerInfo to the boolean variable passed }
  GBPlayerInfo.Enabled := Not bLogical;
End;

Procedure TfrmPlayerLookUp.pGBMasterPointsReadOnly(bLogical: Boolean);
Begin
  { pGBMasterPointsReadOnly: Sets the enabled property of GBMasterPoints to the boolean variable passed }
  GBMasterPoints.Enabled := Not bLogical;
End;

Procedure TfrmPlayerLookUp.F1KeyExecute(Sender: TObject);
Begin
  pRunExtProg(frmPlayerLookUp, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
End;

Procedure TfrmPlayerLookUp.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = VK_RETURN Then Begin
    key := 0;
    If iDataMode > 0 Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    { At least the Last Name Field must be filled in... }
    FreeHintWin;
    If iDataMode > 0 Then If Trim(OSFLastName.Text) <> '' Then pSaveChanges Else pAbortChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmPlayerLookUp.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    { At least the Last Name Field must be filled in... }
    If iDataMode > 0 Then If Trim(OSFLastName.Text) <> '' Then pSaveChanges Else pAbortChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmPlayerLookUp.ComboBoxChange(Sender: TObject);
Var
  iForLoop, iPos, iTxtLen: Integer;
  sTempStr: String;
Begin
  { COMBOBOXCHANGE: TLMDHeaderListComboBox OnChange Routine }
  If (Sender As TLMDHeaderListComboBox).Focused Then Begin
    With (Sender As TLMDHeaderListComboBox) Do Begin
      sTempStr := Text;
      For iForLoop := 0 To (ListBox.Items.Count - 1) Do Begin
        iPos := Pos(sTempStr, ListBox.Items[iForLoop]);
        iTxtLen := Length(sTempStr);
        If (iPos = 1) Or (iPos = 2) Then Begin
          ListBox.ItemIndex := iForLoop;
          Break;
        End;
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.ComboBoxEnter(Sender: TObject);
Begin
  { COMBOBOXENTER: TLMDHeaderListComboBox OnEnter Routine }
  With (Sender As tLMDHeaderListComboBox) Do Begin
    DroppedDown := True;
    ListBox.AlternateColors := True;
    Refresh;
    If ListBox.ItemIndex <> -1 Then ListBox.ItemIndex := fFindCBItemIndex(Sender, Text)
    Else ListBox.ItemIndex := 1;
  End;
End;

Function TfrmPlayerLookUp.fFindCBItemIndex(cbName: TObject; sText2Find: String): Integer;
Var
  iForLoop: Integer;
  sTempStr: String;
Begin
  { FFINDCBITEMINDEX: Used to see if there is a matching entry in the current
    TLMDHeaderListComboBox object. Returns the index position if there is or 0 if not. }
  Result := 1;
  With (cbName As TLMDHeaderListComboBox) Do Begin
    sTempStr := Text;
    For iForLoop := 0 To (ListBox.Items.Count - 1) Do Begin
      If Pos(sTempStr, ListBox.Items[iForLoop]) = 1 Then Begin
        Result := iForLoop;
        Break;
      End;
    End;
  End;
End;

Function duplicate_player(Const number: String; Fno, Kno: integer; Func: String): boolean;
Var
  tempkey: keystr;
  trecno: longint;
  Pn: String[7];
Begin
  duplicate_player := false;
  pn := number;
  If Not player_check(pn, pnPoundACBL) Then exit;
  tempkey := Pads(player_key(number), 7);
  findkey(IdxKey[Fno, Kno]^, trecno, tempkey);
  If ok
    And ((trecno <> recno[Playern_no]) Or ((func = 'A') Or (func = 'C'))) Then
    duplicate_player := true;
  ok := true;
End;

Procedure TfrmPlayerLookUp.LMDHLCBEnter(Sender: TObject);
Begin
  { TLMDHLCBCountry OnEnter Routine }
  With (Sender As tLMDHeaderListComboBox) Do Begin
    ListBox.AlternateColors := True;
  End;
End;

Procedure TfrmPlayerLookUp.pDateExit(Sender: TObject);
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
End;

Procedure TfrmPlayerLookUp.FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End;
End;

Procedure TfrmPlayerLookUp.FormKeyUp(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End;
End;

Procedure TfrmPlayerLookUp.MailingAddressCheck;
Var
  bGoodMailingAddress: Boolean;
Begin
  bGoodMailingAddress := True;
  If Trim(OSFStreet1.Text) = '' Then bGoodMailingAddress := False;
  If Trim(OSFCity.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(LMDHLCBState.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(LMDHLCBState.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(OPFZip.Text) <> '' Then bGoodMailingAddress := False;
  If bGoodMailingAddress Then Begin
    If Trim(LMDHLCBMail.Text) = '' Then LMDHLCBMail.Text := 'M';
  End;
End;

Procedure TfrmPlayerLookUp.pAbortChanges;
Begin
  { Set Events for Edit Fields }
  pSetEvents(False);
  { Turn the Player Info Data Area OFF }
  pPlayerInfo(False);
  //-------------------------------------------------------------------------------------
  If iDataMode = 1 Then Begin // If adding a record
    ClearKey(IdxKey[Playern_no, 1]^);
    { Find the first available record and load it's values to the fields on the screen }
    pNextPNRecord;
  End
  Else Begin
    GetARec(PlayerN_no); // Store the Record
    { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
    pFillPLUFields;
  End;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  bEscKeyUsed := False;
  If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
End;

Function TfrmPlayerLookUp.Status_OK(Const Fno: Integer): Boolean;
Var
  TOK: Boolean;
Begin
  TOK := True;
  TOK := (UsedRecs(DatF[Fno]^) > 0);
  Status_OK := TOK
End;

Function TfrmPlayerLookUp.fGetLength(iFileNo: Integer; sFieldName: String): Integer;
Begin
  Result := dbSizes.Playern_no
End;

Procedure TfrmPlayerLookUp.pCheckEditFields;
Begin
  // -----------------------------------------------------------
  { The field CatA is not used in the Player Lookup Module! }
  // -----------------------------------------------------------
  { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
    in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
    with the changed data. The PLAYERN dataset is used by all record saving, adding and copying
    routines in DBASE.PAS, to update the actual PLAYERN.DAT table.}
  { Pad fields with 40 blank spaces, since 40 characters is the widest field length and when
  a record field is stored, extra spaces will be stripped off. }
  Playern.LAST_NAME := Pad(OSFLastName.Text,16);
  Playern.FIRST_NAME := Pad(OSFFirstName.Text,16);
  Playern.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  Playern.STREET1 := Pad(OSFStreet1.Text,30);
  Playern.STREET2 := Pad(OSFStreet2.Text,26);
  Playern.CITY := Pad(OSFCity.Text,16);
  Playern.STATE := Pad(LMDHLCBState.Text,2);
  Playern.ZIP := Pad(OPFZip.Text,10);
  Playern.COUNTRY := Pad(LMDHLCBCountry.Text,2);
  Playern.EMAIL := Pad(OSFEmail.Text,40);
  Playern.PHONE := Pad(OPFPhone.GetStrippedEditString,20);
  Playern.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
  Playern.CAT_B := Pad(OPFCatB.Text,1);
  Playern.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
  Playern.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
  Playern.DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
  Playern.GENDER := Pad(LMDHLCBGender.Text,1);
  Playern.FEE_TYPE := Pad(LMDHLCBFee.Text,1);
  Playern.PAID_THRU := Pad(OPFPaidThru.GetStrippedEditString,6);
End;

Function TfrmPlayerLookUp.fCheckForChanges: Boolean;
Var
  osPlayer_no: OpenString;
Begin
  Result := false;
  // -----------------------------------------------------------
  { The field CatA is not used in the Player Lookup Module! }
  // -----------------------------------------------------------
  { Check to see if the player number has changed, then check to make sure it is not a duplicate
    player number. }
  If trim(OSFPlayerNo.Text) <> trim(Playern.PLAYER_NO) Then Begin
    osPlayer_no := OSFPlayerNo.Text;
    If Not (player_check(osPlayer_no, pnAny)) Then Begin
      MessageDlg('Invalid ACBL Player Number!', mtError, [mbOK], 0);
      Result := True;
    End;
  End;

  If Trim(OSFLastName.Text) <> Trim(Playern.LAST_NAME) Then Result := True;
  If Trim(OSFFirstName.Text) <> Trim(Playern.FIRST_NAME) Then Result := True;
  If Trim(OSFStreet1.Text) <> Trim(Playern.STREET1) Then Result := True;
  If Trim(OSFStreet2.Text) <> Trim(Playern.STREET2) Then Result := True;
  If Trim(OSFCity.Text) <> Trim(Playern.CITY) Then Result := True;
  If Trim(LMDHLCBState.Text) <> Trim(Playern.STATE) Then Result := True;
  If Trim(OPFZip.Text) <> Trim(Playern.ZIP) Then Result := True;
  If Trim(LMDHLCBCountry.Text) <> Trim(Playern.COUNTRY) Then Result := True;
  If Trim(OSFEmail.Text) <> Trim(Playern.EMAIL) Then Result := True;
  If OPFPhone.GetStrippedEditString <> Playern.PHONE Then Result := True;
  If LMDHLCBRank.Text <> Playern.ACBL_RANK Then Result := True;
  If OPFCatB.Text <> Playern.CAT_B Then Result := True;
  If LMDHLCBMail.Text <> Playern.MAIL_CODE Then Result := True;
  If Trim(OSFUnitNo.Text) <> Trim(Playern.UNIT_NO) Then Result := True;
  If Trim(OSFDistrictNo.Text) <> Trim(Playern.DISTRICT_NO) Then Result := True;
  If LMDHLCBGender.Text <> Playern.GENDER Then Result := True;
  If LMDHLCBFee.Text <> Playern.FEE_TYPE Then Result := True;
  If OPFPaidThru.GetStrippedEditString <> Playern.PAID_THRU Then Result := True;
End;

Procedure TfrmPlayerLookUp.pCopyRecData;
Begin
  // -----------------------------------------------------------
  { The field CatA is not used in the Player Lookup Module! }
  // -----------------------------------------------------------
  { PCOPYRECDATA: Saves all data entry fields to their correct position in the PLAYERN dataset.
    The PLAYERN dataset is used by all record saving, adding and copying routines in DBASE.PAS,
    to update the actual PLAYERN.DAT table. }
  Playern.LAST_NAME := Pad(OSFLastName.Text,16);
  Playern.FIRST_NAME := Pad(OSFFirstName.Text,16);
  Playern.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  Playern.STREET1 := Pad(OSFStreet1.Text,30);
  Playern.STREET2 := Pad(OSFStreet2.Text,26);
  Playern.CITY := Pad(OSFCity.Text,16);
  Playern.STATE := Pad(LMDHLCBState.Text,2);
  Playern.ZIP := Pad(OPFZip.Text,10);
  Playern.COUNTRY := Pad(LMDHLCBCountry.Text,2);
  Playern.EMAIL := Pad(OSFEmail.Text,40);
  { Both Phone Numbers are stored in one field, Phone Number 1 first 10 characters,
    Phone Number 2 second 10 characters}
  Playern.PHONE := Pad(OPFPhone.GetStrippedEditString,20);
  Playern.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
  Playern.CAT_B := Pad(OPFCatB.Text,1);
  Playern.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
  Playern.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
  Playern.DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
  Playern.GENDER := Pad(LMDHLCBGender.Text,1);
  Playern.FEE_TYPE := Pad(LMDHLCBFee.Text,1);
  Playern.PAID_THRU := Pad(OPFPaidThru.GetStrippedEditString,6);
End;

Procedure TfrmPlayerLookUp.pSaveChanges;
Begin
  { Set Events for Edit Fields }
  pSetEvents(False);
  Refresh;
  dbase.Filno := Playern_no;
  Case iDataMode Of
    1, 3: Begin
        If Not duplicate_player(OSFPlayerNo.Text, Playern_no, 2, 'C') Then Begin
          MailingAddressCheck;
          pCopyRecData;
          dbase.Add_Record;
          { Save a copy of the Added or Copied record to the PLChange Table }
          pAddToPlChange(True);
        End
        Else Begin
          If (MessageDlg('This ACBL Player Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFPlayerNo.SetFocus;
            Exit;
          End
          Else pAbortChanges; // Adding or Copying a record
        End;
      End;
    2: Begin
        If fCheckForChanges Then Begin
          { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
            in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
            with the changed data. }
          MailingAddressCheck;
          pCheckEditFields;
          dbase.PutARec(Playern_no);
          { Save a copy of the Edited record to the PLChange Table }
          pAddToPlChange(True);
        End
        Else pAbortChanges; // Editing a record
      End;
  End;
  // Turn the Player Info Data Area OFF
  pPlayerInfo(False);
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 0;
  sDataMode := null;
  bEscKeyUsed := False;
  pUpdateStatusBar;
End;

Procedure TfrmPlayerLookUp.pPlayerInfo(bEnable: Boolean);
Begin
  pResetFldAttrib;
  { Set Events for Edit Fields }
  pSetEvents(bEnable);
  { Allow or Deny access to the Player Information fields }
  { Set the state of ActionManager1 for the form players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  If Not bEnable Then GBMenu.SetFocus;
  GBPlayerInfo.Enabled := bEnable;
  // ----------------------------------------------------------------------
  GBMasterPoints.Visible := Not bEnable;
  // ----------------------------------------------------------------------
  LMDButPlayerInfoDone.Enabled := bEnable;
  LMDButPlayerInfoDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmPlayerLookUp.pUpdateStatusBar;
Begin
  iReccount := UsedRecs(DatF[Playern_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'PLAYERN.DAT') + ' - Contains ' +
    Trim(IntToStr(iReccount)) + ' Records.';
End;

Procedure TfrmPlayerLookUp.ButDeleteActionExecute(Sender: TObject);
Begin
  Filno := Playern_no;
  Delete_Record(True, frmPlayerLookUp.HelpContext);
  pUpdateStatusBar;
  pFillPLUFields;
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmPlayerLookUp.ButEditActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { In module Dbase call the procedure PreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  PreEditSaveKeys;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Player Info Data Area ON }
  pPlayerInfo(True);
  { Put the cursor in the first field }
  OSFLastName.SetFocus;
End;

Procedure TfrmPlayerLookUp.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  Refresh;
  dbase.Filno := Playern_no;
  Case iDataMode Of
    1, 3: Begin
        // function duplicate_player(const number: String; Fno,Kno: integer; Func: String)
        If Not duplicate_player(OSFPlayerNo.Text, Playern_no, 2, 'C') Then Begin
          MailingAddressCheck;
          pCopyRecData;
          dbase.Add_Record;
          { Save a copy of the Added or Copied record to the PLChange Table }
          pAddToPlChange(True);
        End
        Else Begin
          If (MessageDlg('This ACBL Player Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFPlayerNo.SetFocus;
            Exit;
          End
          Else pAbortChanges; // Adding or Copying a record
        End;
      End;
    2: Begin
        If fCheckForChanges Then Begin
          { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
            in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
            with the changed data. }
          MailingAddressCheck;
          pCheckEditFields;
          bKeyIsUnique := PostEditFixKeys;
          If Not bKeyIsUnique Then Begin
            If (MessageDlg('This record already exists in this database.', mtError,
              [mbOK], 0) = mrOK) Then Begin
              OSFLastName.SetFocus;
              Exit;
            End
            Else pAbortChanges;
          End
          Else Begin
            dbase.PutARec(Playern_no);
            { Save a copy of the Edited record to the PLChange Table }
            pAddToPlChange(True);
          End;
        End
        Else pAbortChanges; // Editing a record
      End;
  End;
  pResetFldAttrib;
  { Turn the Player Info Data Area OFF }
  pPlayerInfo(False);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  bEscKeyUsed := False;
  pUpdateStatusBar;
End;

Procedure TfrmPlayerLookUp.pAddToPlChange(Const AskUser: boolean);
Var
  Tempkey, ksFixKeys: keystr;
  iForLoop: Integer;
Begin
  If Not Player_Check(Playern.Player_no, pnACBL) Then Begin
    If AskUser Then Begin
      ErrBox('Transaction file only allowed for players with ACBL numbers.', MC, 0);
    End;
    exit;
  End;
  Edit_Add := true;
  Tempkey := GetKey(Playern_no, 2);
  Filno := PlChange_no;
  FindKey(IdxKey[Filno, 2]^, Recno[Filno], TempKey);
  If ok Then Begin
    GetARec(Filno);
    Edit_Add := false;
    DelARec(Filno);
    Edit_Add := true;
  End;
  ok := true;
  // ----------------------------------------------------------------------------
  { Copy the Playern Record to the PLChange Record }
  Plchange := Playern;
  { Save the Changes back to the Database }
  Add_Record;
  Filno := Playern_no;
  If AskUser Then ErrBox('Player transfered to transaction file.', MC, 0);
End;

Procedure TfrmPlayerLookUp.FlushAFile(Const Fno: Integer);
Var
  i, Keyno: Integer;
Begin
  FlushFile(DatF[Fno]^);
  For Keyno := 1 To MaxKeyno Do
    If (KeyLen[Fno, Keyno] > 0) Then FlushIndex(IdxKey[Fno, Keyno]^);
End;

Procedure TfrmPlayerLookUp.F5KeyExecute(Sender: TObject);
Begin
  pAddToPlChange(True);
End;

Procedure TfrmPlayerLookUp.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  { Turn the Player Info Data Area ON }
  pPlayerInfo(True);
  //PlayernInit;
  InitRecord(FilNo);
  pFillPLUFields;
  { Put the cursor in the first field }
  OSFLastName.SetFocus;
End;

Procedure TfrmPlayerLookUp.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  sDataMode := 'C';
  { Turn the Player Info Data Area ON }
  pPlayerInfo(True);

  Add_Init;
  pFillPLUFields;
  { Put the cursor in the first field }
  OSFLastName.SetFocus;
End;

Procedure TfrmPlayerLookUp.pSetEvents(bSwitchEventsOn: Boolean);
Begin
  If bSwitchEventsOn Then Begin
    OSFLastName.OnExit := OSFLastNameExit;
    OSFPlayerNo.OnExit := OSFPlayerNoExit;
    LMDHLCBState.OnExit := LMDHLCBStateExit;
    LMDHLCBRank.OnExit := LMDHLCBRankExit;
    LMDHLCBMail.OnExit := LMDHLCBMailExit;
    LMDHLCBGender.OnExit := LMDHLCBGenderExit;
    LMDHLCBFee.OnExit := LMDHLCBFeeExit;
    OSFDistrictNo.OnExit := OSFDistrictNoExit;
  End
  Else Begin
    OSFLastName.OnExit := Nil;
    OSFPlayerNo.OnExit := Nil;
    LMDHLCBState.OnExit := Nil;
    LMDHLCBRank.OnExit := Nil;
    LMDHLCBMail.OnExit := Nil;
    LMDHLCBGender.OnExit := Nil;
    LMDHLCBFee.OnExit := Nil;
    OSFDistrictNo.OnExit := Nil;
  End;
End;

Procedure TfrmPlayerLookUp.F3KeyExecute(Sender: TObject);
Begin
  // ---------------------------------------------------
  ActionManager1.State := asSuspended;
  frmPlayerLookUp.Hide;
  // ---------------------------------------------------
  frmTDInfo := tfrmTDInfo.Create(Nil);
  frmTDInfo.ShowModal;
  frmTDInfo.Free;
  // ---------------------------------------------------
  ActionManager1.State := asNormal;
  frmPlayerLookUp.Show;
  pFillPLUFields;
  // ---------------------------------------------------
  If bWinNormal Then WindowState := wsNormal
  Else WindowState := wsMaximized;
End;

Procedure TfrmPlayerLookUp.AlignFile(Const Fno, Kno: byte; Const KeyToFind: String;
  Const ShowRec: boolean);
Var
  Tempkey: KeyStr;
Begin
  Tempkey := KeytoFind;
  FindKey(fIdxKey[Fno, Kno]^, fRecno[Fno], tempkey);
  If ok Then fGetARec(Fno);
End;

Procedure TfrmPlayerLookUp.F4KeyExecute(Sender: TObject);
Var
  Tempkey: KeyStr;
Begin
  FilNo := PlChange_no;
  // ---------------------------------------------------
  ActionManager1.State := asSuspended;
  frmPlayerLookUp.Hide;
  // ---------------------------------------------------
  frmPLChange := tfrmPLChange.Create(Nil);
  frmPLChange.ShowModal;
  frmPLChange.Free;
  // ---------------------------------------------------
  ActionManager1.State := asNormal;
  frmPlayerLookUp.Show;
  // ---------------------------------------------------
  FilNo := Playern_no;
  // ---------------------------------------------------
  Tempkey := ksPCPlayerNo;
  FindKey(IdxKey[Filno, 2]^, Recno[Filno], tempkey);
  GetARec(Filno);
  pFillPLUFields;
  If bWinNormal Then WindowState := wsNormal
  Else WindowState := wsMaximized;
End;

Procedure TfrmPlayerLookUp.pDateFieldKeyPress(Sender: TObject; Var Key: Char);
Begin
  pEditFieldKeyPress(Sender, Key);
End;

Procedure TfrmPlayerLookUp.OPFPYTDExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[4] := OPFPYTD.AsExtended;
  J := 4;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
End;

Function TfrmPlayerLookUp.fShowPoints(Const Ptype: byte): String;
Var
  num: longint;
Begin
  If UsedRecs(DatF[Playern_no]^) < 1 Then num := 0
  Else With playern Do Begin
      If Ptype < 1 Then num := Key2Num(Elig_pts)
      Else Begin
        num := Key2Num(Tourn_pts);
        If Ptype > 1 Then Inc(num, Key2Num(M_T_D));
        If Ptype > 2 Then Inc(num, Key2Num(Y_T_D));
        If Ptype > 3 Then Inc(num, Key2Num(Tot_points));
      End;
    End;
  fShowPoints := Real2Str(num / 100.0, 8, 2);
End;

Procedure TfrmPlayerLookUp.SetPoints(Const j: byte);
Var
  num: longint;
Begin
  With playern Do Begin
    If j > 2 Then Begin
      If appoints[j] < aPpoints[j - 1] Then aPpoints[j] := aPpoints[j - 1];
      num := Round((aPpoints[j] - aPpoints[j - 1]) * 100.0);
      If (j = 5) And (aPpoints[j] < aPpoints[1]) Then aPpoints[j] := aPpoints[1];
    End
    Else num := Round(aPpoints[j] * 100.0);
    Case j Of
      1: Elig_pts := Num2Key(num, 4);
      2: Tourn_pts := Num2Key(num, 4);
      3: M_T_D := Num2Key(num, 4);
      4: Y_T_D := Num2Key(num, 4);
      5: Tot_points := Num2Key(num, 4);
    End;
  End;
End;

Procedure TfrmPlayerLookUp.FixPoints(Const j: byte);
Var
  pp: real;
  k: byte;
Begin
  If j = 1 Then aPpoints[5] := aPpoints[5] + aPpoints[1] - rOldepts;
  If j < 3 Then exit;
  If aPpoints[j - 1] > aPpoints[j] Then aPpoints[j - 1] := aPpoints[j];
  If (j = 5) And (aPpoints[1] > aPpoints[5]) Then aPpoints[1] := aPpoints[5];
  FixPoints(j - 1);
End;

Procedure TfrmPlayerLookUp.pStoreMPoints;
Var
  j: byte;
Begin
  With playern Do Begin
    aOldpoints[1] := Elig_pts;
    aOldpoints[2] := Tourn_pts;
    aOldpoints[3] := M_T_D;
    aOldpoints[4] := Y_T_D;
    aOldpoints[5] := Tot_points;
    For j := 1 To 5 Do Begin
      aPpoints[j] := Key2Num(aOldpoints[j]) / 100.0;
      If j = 1 Then roldepts := aPpoints[1];
      If j > 2 Then aPpoints[j] := aPpoints[j] + aPpoints[j - 1];
    End;
  End;
End;

Procedure TfrmPlayerLookUp.pRefreshMPFields;
Begin
  OPFPEligibility.Text := Real2Str((aPpoints[1]), 9, 2);
  OPFPRecent.Text := Real2Str((aPpoints[2]), 9, 2);
  OPFPMTD.Text := Real2Str((aPpoints[3]), 9, 2);
  OPFPYTD.Text := Real2Str((aPpoints[4]), 9, 2);
  OPFPTotal.Text := Real2Str((aPpoints[5]), 9, 2);
End;

Procedure TfrmPlayerLookUp.OPFPMTDExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[3] := OPFPMTD.AsExtended;
  J := 3;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
End;

Procedure TfrmPlayerLookUp.OPFPRecentExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[2] := OPFPRecent.AsExtended;
  J := 2;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
End;

Procedure TfrmPlayerLookUp.Action1Execute(Sender: TObject);
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinNormal := (WindowState = wsNormal);
End;

Procedure TfrmPlayerLookUp.Action2Execute(Sender: TObject);
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinNormal := (WindowState = wsNormal);
End;

Procedure TfrmPlayerLookUp.Action3Execute(Sender: TObject);
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinNormal := (WindowState = wsNormal);
End;

Procedure TfrmPlayerLookUp.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmPlayerLookUp.Action5Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmPlayerLookUp.Action6Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmPlayerLookUp.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
Begin
  If UpperCase(sType) = 'S' Then
    pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
  Else If UpperCase(sType) = 'P' Then
    pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
  Else If UpperCase(sType) = 'C' Then
    pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
  Else If UpperCase(sType) = 'D' Then
    pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
  Else If UpperCase(sType) = 'H' Then
    pLMDHCBFieldAvail(bDoedit, (oSender As TLMDHeaderListComboBox))
  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
  // -------------------------------------------------------------------------------------------------------
  If bDisplay_Rec Then Begin
    { Load all information to the appropriate field from the current record }
    pFillPLUFields;
  End;
  // -------------------------------------------------------------------------------------------------------
  If bWasOnEnter Then Begin
    If UpperCase(sType) = 'S' Then Begin
      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'P') Then Begin
      If Not (oSender As TOvcPictureField).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'D') Then Begin
      If Not (oSender As TOvcDateEdit).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'H') Then Begin
      If Not (oSender As TLMDHeaderListComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'C') Then Begin
      If Not (oSender As TOvcComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End;
  End;
End;

Procedure TfrmPlayerLookUp.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayerLookUp.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayerLookUp.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayerLookUp.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayerLookUp.pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayerLookUp.pResetFldAttrib;
Var
  iComponent: integer;
Begin
  With Self Do Begin
    For iComponent := 0 To ComponentCount - 1 Do Begin
      If Components[iComponent].ClassType = TOvcSimpleField Then
        pOSFieldAvail(True, (Components[iComponent] As TOvcSimpleField))
      Else If Components[iComponent].ClassType = TOvcPictureField Then
        pOPFieldAvail(True, (Components[iComponent] As TOvcPictureField))
      Else If Components[iComponent].ClassType = TOvcDateEdit Then
        pODEFieldAvail(True, (Components[iComponent] As TOvcDateEdit))
      Else If Components[iComponent].ClassType = TOvcComboBox Then
        pOCBFieldAvail(True, (Components[iComponent] As TOvcComboBox));
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFLastNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.LAST_NAME := Pad(OSFLastName.Text,16);
  bDoEdit := CustomPLayerEdit(Scrno, 3, Playern.LAST_NAME, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFLastNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      { Validation of Last Name field when exiting it }
      If trim(OSFLastName.Text) = '' Then Begin
        If MessageDlg('Last Name must NOT be blank! ' + #13 + #10 + '' + #13 + #10 +
          '        Correct this error?', mtError, [mbYes, mbNo], 0) = MRNO Then Begin
          pAbortChanges;
        End
        Else OSFLastName.SetFocus;
      End
      Else Begin
        Playern.LAST_NAME := Pad(OSFLastName.Text,16);
        bDoEdit := CustomPLayerEdit(Scrno, 3, Playern.LAST_NAME, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFFirstNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.FIRST_NAME := Pad(OSFFirstName.Text,16);
  bDoEdit := CustomPLayerEdit(Scrno, 2, Playern.FIRST_NAME, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFFirstNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.FIRST_NAME := Pad(OSFFirstName.Text,16);
      bDoEdit := CustomPLayerEdit(Scrno, 2, Playern.FIRST_NAME, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFPlayerNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  bDoEdit := CustomPLayerEdit(Scrno, 1, Playern.PLAYER_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFPlayerNoExit(Sender: TObject);
Var
  osPlayer_no: OpenString;
  DataMode: Char;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      osPlayer_no := OSFPlayerNo.Text;
      DataMode := sDataMode;
      { Check for a duplicate Player Number }
      If Not (fcheck_player(osPlayer_no, DataMode)) Then Begin
        If PlayerError = 2 Then
          ErrBox('This ACBL Player Number already exists in this database!', mc, 0)
        Else If PlayerError = 1 Then ErrBox('Invalid ACBL Player Number!', mc, 0);
        OSFPlayerNo.SetFocus;
      End
      Else Begin
        Playern.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
        bDoEdit := CustomPLayerEdit(Scrno, 1, Playern.PLAYER_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFStreet1Enter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.Street1 := Pad(OSFStreet1.Text,30);
  bDoEdit := CustomPLayerEdit(Scrno, 4, Playern.Street1, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFStreet1Exit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.Street1 := Pad(OSFStreet1.Text,30);
      bDoEdit := CustomPLayerEdit(Scrno, 4, Playern.Street1, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFStreet2Enter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.Street2 := Pad(OSFStreet2.Text,26);
  bDoEdit := CustomPLayerEdit(Scrno, 5, Playern.Street2, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFStreet2Exit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.Street2 := Pad(OSFStreet2.Text,26);
      bDoEdit := CustomPLayerEdit(Scrno, 5, Playern.Street2, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFCityEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.CITY := PaD(OSFCity.Text,16);
  bDoEdit := CustomPLayerEdit(Scrno, 6, Playern.CITY, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFCityExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.CITY := Pad(OSFCity.Text,16);
      bDoEdit := CustomPLayerEdit(Scrno, 6, Playern.CITY, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPlayerLookUp.LMDHLCBStateEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.STATE := Pad(LMDHLCBState.Text,2);
  bDoEdit := CustomPLayerEdit(Scrno, 7, Playern.STATE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayerLookUp.LMDHLCBStateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidStateCode(Playern.Country, LMDHLCBState.Text)) Then Begin
        ErrBox('Invalid State/Province code.', mc, 0);
        LMDHLCBState.Text := '';
        LMDHLCBCountry.Text := '';
        LMDHLCBState.SetFocus;
      End
      Else Begin
        Playern.STATE := Pad(LMDHLCBState.Text,3);
        bDoEdit := CustomPLayerEdit(Scrno, 7, Playern.STATE, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'H');
        LMDHLCBCountry.Text := Playern.Country;
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OPFZipEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.ZIP := Pad(OPFZip.Text,10);
  bDoEdit := CustomPLayerEdit(Scrno, 8, Playern.ZIP, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPlayerLookUp.OPFZipExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.ZIP := Pad(OPFZip.Text,10);
      bDoEdit := CustomPLayerEdit(Scrno, 8, Playern.ZIP, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmPlayerLookUp.LMDHLCBCountryEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.Country := Pad(LMDHLCBCountry.Text,2);
  bDoEdit := CustomPLayerEdit(Scrno, 9, Playern.Country, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayerLookUp.LMDHLCBCountryExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidCountryCode(LMDHLCBCountry.Text, true)) Then Begin
        ErrBox('Invalid Country Code', mc, 0);
        LMDHLCBCountry.Text := '';
        LMDHLCBCountry.SetFocus;
      End
      Else Begin
        Playern.Country := Pad(LMDHLCBCountry.Text,2);
        bDoEdit := CustomPLayerEdit(Scrno, 9, Playern.Country, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'H');
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFEmailEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.email := Pad(OSFEmail.Text,40);
  bDoEdit := CustomPLayerEdit(Scrno, 10, Playern.email, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFEmailExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (Empty(OSFEmail.Text) Or (ValidEmailAddress(OSFEmail.Text, 40) > 0)) Then Begin
        ErrBox('Email address is not valid', mc, 0);
        OSFEmail.SetFocus;
      End
      Else Begin
        Playern.email := Pad(OSFEmail.Text,40);
        bDoEdit := CustomPLayerEdit(Scrno, 10, Playern.email, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.LMDHLCBRankEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 12, Playern.ACBL_RANK, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayerLookUp.LMDHLCBRankExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Ranking Codes:

         Rookie (blank)     0 - 5
      A  Junior Master      5 - 20
      B  Club Master       20 - 50
      C  Sectional Master  50 - 100
      D  Regional Master  100 - 200
      E  NAC Master       200 - 300
      F  Life Master      300 - 500
      G  Bronze LM        500 - 1000
      H  Silver LM       1000 - 2500
      I  Gold LM         2500 - 5000
      J  Diamond LM      5000 - 7500
      K  Emerald LM      7500 - 10000
      L  Platinum LM     over   10000
      M  Grand LM NABC   over   10000
      X  Rank not known
      S  Suspended
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
      bDoEdit := CustomPLayerEdit(Scrno, 12, Playern.ACBL_RANK, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBRank.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sRankCode) Do Begin
          If LMDHLCBRank.Text = libdata.sRankCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then Begin
        ErrBox('Invalid ACBL rank', mc, 0);
        LMDHLCBRank.Text := '';
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.LMDHLCBMailEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 11, Playern.MAIL_CODE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayerLookUp.LMDHLCBMailExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Mail Codes:

        (blank) Mailing label will NOT be printed
      M Mailing label will be printed
      U Address unknown
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
      bDoEdit := CustomPLayerEdit(Scrno, 11, Playern.MAIL_CODE, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBMail.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sMailCode) Do Begin
          If LMDHLCBMail.Text = libdata.sMailCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then Begin
        ErrBox('Mail code must blank or M or U', mc, 0);
        LMDHLCBMail.Text := '';
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFUnitNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
  bDoEdit := CustomPLayerEdit(Scrno, 19, Playern.UNIT_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFUnitNoExit(Sender: TObject);
Var
  osUnitNo, osDistNo: OpenString;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      osUnitNo := OSFUnitNo.Text;
      osDistNo := OSFDistrictNo.Text;
      If Not (Groups.ValidUnit(osUnitNo, osDistNo)) Then Begin
        ErrBox('Invalid Unit number', mc, 0);
        OSFUnitNo.Text := '';
        OSFDistrictNo.Text := '';
        OSFUnitNo.SetFocus;
      End
      Else Begin
        Playern.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
        OSFDistrictNo.Text := osDistNo;
        Playern.District_no := FixNumField(OSFDistrictNo.Text,2);
        bDoEdit := CustomPLayerEdit(Scrno, 19, Playern.UNIT_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OSFDistrictNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.District_no := FixNumField(OSFDistrictNo.Text,2);
  bDoEdit := CustomPLayerEdit(Scrno, 20, Playern.District_no, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayerLookUp.OSFDistrictNoExit(Sender: TObject);
Var
  osDistrictNo: OpenString;
  //  bValidDistrictNo: Boolean;
Begin
  osDistrictNo := OSFDistrictNo.Text;
  //  bValidDistrictNo := LibData.fValidDistrict(osDistrictNo);
  //  If Not bValidDistrictNo Then OSFDistrictNo.SetFocus;
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (fValidDistrict(osDistrictNo)) Then Begin
        ErrBox('Invalid District number', mc, 0);
        OSFDistrictNo.Text := '';
        OSFDistrictNo.SetFocus;
      End
      Else Begin
        Playern.District_no := FixNumField(OSFDistrictNo.Text,2);
        bDoEdit := CustomPLayerEdit(Scrno, 20, Playern.District_no, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.LMDHLCBGenderEnter(Sender: TObject);
Begin
  { Used 18 for the field code since it was not assigned in I_Update.Inc }
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.GENDER := Pad(LMDHLCBGender.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 13, Playern.GENDER, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayerLookUp.LMDHLCBGenderExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Gender Codes:

        (blank) Unknown
      F Female
      M Male
  }
  { Used 18 for the field code since it was not assigned in I_Update.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.GENDER := Pad(LMDHLCBGender.Text,1);
      bDoEdit := CustomPLayerEdit(Scrno, 13, Playern.GENDER, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBGender.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sGenderCode) Do Begin
          If LMDHLCBGender.Text = libdata.sGenderCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then Begin
        ErrBox('Gender must be ( )Blank, (M)ale or (F)emale', mc, 0);
        LMDHLCBGender.SetFocus;
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.LMDHLCBFeeEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.Fee_type := Pad(LMDHLCBFee.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 22, Playern.Fee_type, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayerLookUp.LMDHLCBFeeExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Fee Codes:

        (blank) None of the above
      H Household
      J Student
      L Life master reduced
      M LM paying regular fees
      N Non life master regular
      P Patron
      X Patron Household
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.Fee_type := Pad(LMDHLCBFee.Text,1);
      bDoEdit := CustomPLayerEdit(Scrno, 22, Playern.Fee_type, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If (LMDHLCBFee.Focused) Then Begin
        If LMDHLCBFee.Text <> ' ' Then Begin
          bValid := False;
          For iForLoop := 1 To length(libdata.sFeeCode) Do Begin
            If LMDHLCBFee.Text = libdata.sFeeCode[iForLoop] Then Begin
              bValid := True;
              break
            End;
          End;
        End
        Else bValid := True;
        If Not bValid Then Begin
          ErrBox('Invalid Fee Code', mc, 0);
          LMDHLCBFee.SetFocus;
        End;
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OPFCatBEnter(Sender: TObject);
Begin
  { Used 13 for the field code since it was not assigned in I_Update.Inc }
  bDoEdit := True;
  Playern.CAT_B := Pad(OPFCatB.Text,1);
  bDoEdit := CustomPLayerEdit(Scrno, 21, Playern.CAT_B, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPlayerLookUp.OPFCatBExit(Sender: TObject);
Begin
  { Used 13 for the field code since it was not assigned in I_Update.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((OPFCatB.Text = ' ') Or (OPFCatB.Text = 'P') Or (OPFCatB.Text = 'E')) Then Begin
        ErrBox('Paid pending / Exempt must be P or E or blank', mc, 0);
        OPFCatB.Text := '';
        OPFCatB.SetFocus;
      End
      Else Begin
        Playern.CAT_B := Pad(OPFCatB.Text,1);
        bDoEdit := CustomPLayerEdit(Scrno, 21, Playern.CAT_B, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OPFPaidThruEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.Paid_Thru := Pad(OPFPaidThru.Text,6);
  bDoEdit := CustomPLayerEdit(Scrno, 18, Playern.Paid_Thru, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPlayerLookUp.OPFPaidThruExit(Sender: TObject);
Var
  sPaidThru: String;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      sPaidThru := Copy(OPFPaidThru.Text, 1, 2) + Copy(OPFPaidThru.Text, 4, 4);
      If Not (Empty(sPaidThru) Or chkdate(Copy(sPaidThru, 1, 2) + '01' + Copy(sPaidThru, 3, 4))) Then Begin
        ErrBox('Invalid month/year', mc, 0);
        OPFPaidThru.SetFocus;
      End
      Else Begin
        Playern.Paid_Thru := Pad(OPFPaidThru.Text,6);
        bDoEdit := CustomPLayerEdit(Scrno, 18, Playern.Paid_Thru, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

Procedure TfrmPlayerLookUp.OPFPTotalExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[5] := OPFPTotal.AsExtended;
  J := 5;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
End;

Procedure TfrmPlayerLookUp.ButtonF1KeyClick(Sender: TObject);
Begin
  If HelpContext > 0 Then keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmPlayerLookUp.OPFPhoneEnter(Sender: TObject);
Begin
  bDoEdit := True;
  PLAYERN.PHONE := Pad(OPFPhone.Text,20);
  bDoEdit := CustomPLayerEdit(Scrno, 14, PLAYERN.PHONE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPlayerLookUp.OPFPhoneExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      PLAYERN.PHONE := Pad(OPFPhone.Text,20);
      bDoEdit := CustomPLayerEdit(Scrno, 14, PLAYERN.PHONE, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

function TfrmPlayerLookUp.FormHelp(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
//  if data < 500000 then WinHelp(Application.Handle,'ACBLSCORE.HLP',Help_Context,Data);
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
end;

procedure TfrmPlayerLookUp.FormCreate(Sender: TObject);
begin
  mHHelp := THookHelpSystem.Create(DefChmFile,'',htHHAPI);
end;

procedure TfrmPlayerLookUp.FormDestroy(Sender: TObject);
begin
  mHHelp.Free;
  HHCloseAll;
end;

End.

